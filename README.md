# talthea system
Please consider supporting the development of this system on Patreon here: https://www.patreon.com/swordsandstones

A system and a few compendiums used to play the UESRPG game. Special thanks to 2Minute Tabletop and to drhodesw for the tokens and help creating the compendiums.

Express permission to use the artwork and tokens included in the compendiums of this system was given by 2MinuteTabletop and the copyright holder.

You can find the UESRPG rules here: https://docs.google.com/document/d/1pTgTN2aJUoY95JtquowagfUJLL7tCQYhzJKcCAcbvio/edit

You can find the lively UESRPG Discord Community here: https://discord.gg/KAkXdf9
