/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */


 export class SimpleActorSheet extends ActorSheet {

  /** @override */
	static get defaultOptions() {
	  return mergeObject(super.defaultOptions, {
  	  classes: ["worldbuilding", "sheet", "actor"],
  	  template: "systems/talthea-system/templates/actor-sheet.html",
      width: 780,
      height: 860,
      tabs: [{navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description"}],
      dragDrop: [{dragSelector: [
        ".armor-table .item",
        ".ammunition-table .item",
        ".weapon-table .item",
        ".accessoire-table .item",
        ".spellList .item",
        ".skillList .item",
        ".factionContainer .item",
        ".raceContainer .item",
        ".passifContainer .item",
        ".religionContainer .item",
        ".classeContainer .item",
        ".competenceContainer .item",
        ".effetContainer .item",
        ".languageContainer .item",
        ".talent-container .item",
        ".trait-container .item",
        ".power-container .item",
        ".equipmentList .item",
        ".containerList .item"
      ], 
      dropSelector: null}]
    });
  }

  /* -------------------------------------------- */

  /** @override */

  getData() {
    const  data = super.getData(); 
    data.dtypes = ["String", "Number", "Boolean"];
    data.isGM = game.user.isGM;
    data.editable = data.options.editable;
    const actorData = data.actor;
    data.actor = actorData;
    data.data = actorData.system;
    let options = 0;
    let user = this.user;

    // Prepare Items
    if (this.actor.type === 'character') {
      this._prepareCharacterItems(data);
    }

    return data;
    }

  _prepareCharacterItems(sheetData) {
    const actorData = sheetData.actor;

    //Initialize containers
    const gear = {
      equipped: [],
      unequipped: []
    };
    const weapon = {
      equipped: [],
      unequipped: []
    };
    const accessoire = {
      equipped: [],
      unequipped: []
    };
    const armor = {
      equipped: [],
      unequipped: []
    };
    const power = [];
    const trait = [];
    const talent = [];
    const combatStyle = [];
    const spell = [];
    const skill = [];
    const magicSkill = [];
    const ammunition = {
      equipped: [],
      unequipped: []
    };
    const language = [];
    const race = [];
    const passif = [];
    const religion = [];
    const classe = [];
    const competence = [];
    const effet = [];
    const faction = [];
    const container = [];

    //Iterate through items, allocating to containers
    //let totaWeight = 0;
    for (let i of sheetData.items) {
      let item = i.system;
      i.img = i.img || DEFAULT_TOKEN;
      //Append to item
      if (i.type === 'item') {
        i.system.equipped ? gear.equipped.push(i) : gear.unequipped.push(i)
      }
      //Append to weapons
      else if (i.type === 'weapon') {
          i.system.equipped ? weapon.equipped.push(i) : weapon.unequipped.push(i)
      }
      //Append to accessoires
      else if (i.type === 'accessoire') {
          i.system.equipped ? accessoire.equipped.push(i) : accessoire.unequipped.push(i)
      }
      //Append to armor
      else if (i.type === 'armor') {
        i.system.equipped ? armor.equipped.push(i) : armor.unequipped.push(i)
    }
      //Append to power
      else if (i.type === 'power') {
        power.push(i);
      }
      //Append to trait
      else if (i.type === 'trait') {
        trait.push(i);
      }
      //Append to talent
      else if (i.type === 'talent') {
        talent.push(i);
      }
      //Append to combatStyle
      else if (i.type === 'combatStyle') {
        combatStyle.push(i);
      }
      //Append to spell
      else if (i.type === 'spell') {
        spell.push(i)
      }
      //Append to skill
      else if (i.type === 'skill') {
          skill.push(i);
      }
      //Append to magicSkill
      else if (i.type === 'magicSkill') {
        magicSkill.push(i);
      }
      //Append to ammunition
      else if (i.type === 'ammunition') {
        i.system.equipped ? ammunition.equipped.push(i) : ammunition.unequipped.push(i)
      }
      else if (i.type === "language") {
        language.push(i);
      }
      else if (i.type === "race") {
        race.push(i);
      }
      else if (i.type === "passif") {
        passif.push(i);
      }
      else if (i.type === "religion") {
        religion.push(i);
      }
      else if (i.type === "classe") {
        classe.push(i);
      }
      else if (i.type === "competence") {
        competence.push(i);
      }
      else if (i.type === "effet") {
        effet.push(i);
      }
      //Append to faction
      else if (i.type === "faction") {
        faction.push(i);
      }
      //Append to container
      else if (i.type === "container") {
        container.push(i);
      }
    }

    // Alphabetically sort all item lists
    if (game.settings.get('talthea-system', 'sortAlpha')) {
      const itemCats = [
        gear.equipped, 
        gear.unequipped, 
        weapon.equipped,
        weapon.unequipped, 
        accessoire.equipped,
        accessoire.unequipped, 
        armor.equipped, 
        armor.unequipped, 
        power, 
        trait, 
        talent, 
        combatStyle, 
        spell, 
        skill, 
        magicSkill, 
        ammunition.equipped,
        ammunition.unequipped, 
        language, 
        competence, 
        classe, 
        race,  
        passif,  
        effet, 
        religion, 
        faction,
        container
      ]

      for (let category of itemCats) {
        if (category.length > 1 && category != spell) {
          category.sort((a,b) => {
            let nameA = a.name.toLowerCase()
            let nameB = b.name.toLowerCase()
            if (nameA > nameB) {return 1}
            else {return -1}
          })
        }
        else if (category == spell) {
          if (category.length > 1) {
            category.sort((a, b) => {
              let nameA = a.system.school
              let nameB = b.system.school
              if (nameA > nameB) {return 1}
              else {return -1}
            })
          }
        }
      }
    }

    //Assign and return
    actorData.gear = gear;
    actorData.weapon = weapon;
    actorData.accessoire = accessoire;
    actorData.armor = armor
    actorData.power = power;
    actorData.trait = trait;
    actorData.talent = talent;
    actorData.combatStyle = combatStyle;
    actorData.spell = spell;
    actorData.skill = skill;
    actorData.magicSkill = magicSkill;
    actorData.ammunition = ammunition;
    actorData.language = language;
    actorData.religion = religion;
    actorData.classe = classe;
    actorData.competence = competence;
    actorData.race = race;
    actorData.passif = passif;
    actorData.effet = effet;
    actorData.faction = faction;
    actorData.container = container;

  }

  /* -------------------------------------------- */

  /** @override */
	async activateListeners(html) {
    super.activateListeners(html);

    // Rollable Buttons & Menus
    html.find(".characteristic-roll").click(await this._onClickCharacteristic.bind(this));
    html.find(".skill-roll").click(await this._onSkillRoll.bind(this));
    html.find(".combat-roll").click(await this._onCombatRoll.bind(this));
    html.find(".magic-roll").click(await this._onSpellRoll.bind(this));
    html.find(".resistance-roll").click(this._onResistanceRoll.bind(this));
    html.find(".damage-roll").click(this._onDamageRoll.bind(this));
    html.find(".ammo-roll").click(await this._onAmmoRoll.bind(this));
    html.find(".item-img").click(await this._onTalentRoll.bind(this));
    html.find('.rank-select').click(this._selectCombatRank.bind(this))

    //Update Item Attributes from Actor Sheet
    html.find(".toggle2H").click(await this._onToggle2H.bind(this));
    html.find(".plusQty").click(await this._onPlusQty.bind(this));
    html.find(".minusQty").contextmenu(await this._onMinusQty.bind(this));
    html.find(".itemEquip").click(await this._onItemEquip.bind(this));
    html.find(".wealthCalc").click(await this._onWealthCalc.bind(this));
    html.find(".setBaseCharacteristics").click(await this._onSetBaseCharacteristics.bind(this));
    html.find(".carryBonus").click(await this._onCarryBonus.bind(this));
    html.find(".incrementResource").click(this._onIncrementResource.bind(this))
    html.find(".resourceLabel button").click(this._onResetResource.bind(this))
    html.find("#spellFilter").click(this._filterSpells.bind(this))
    html.find("#itemFilter").click(this._filterItems.bind(this))
    html.find('.equip-items').click(this._onEquipItems.bind(this))

    //Item Create Buttons
    html.find(".item-create").click(await this._onItemCreate.bind(this));

    // Checks for UI Elements on Sheets and Updates
    this._createSpellFilterOptions()
    this._createItemFilterOptions()
    this._setDefaultSpellFilter()
    this._setDefaultItemFilter()
    this._setResourceBars()
    this._createStatusTags()
    this._setDefaultCombatRank()

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Update Inventory Item
    html.find('.item-name').contextmenu(async (ev) => {
      const li = ev.currentTarget.closest('.item')
      const item = this.actor.items.get(li.dataset.itemId)
      this._duplicateItem(item)
    })

    html.find('.item-name').click( async (ev) => {
      const li = ev.currentTarget.closest(".item");
      const item = this.actor.items.get(li.dataset.itemId);
      item.sheet.render(true);
      await item.update({"data.value" : item.system.value})
    });

    // Open Container of item
    html.find('.fa-backpack').click( async (ev) => {
      const li = ev.currentTarget.dataset.containerId
      const item = this.actor.items.get(li);
      item.sheet.render(true);
      await item.update({"data.value" : item.system.value})
    });

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = ev.currentTarget.closest(".item");
      // Detect if the deleted item is a container OR is contained in one
      // Before deleting the item, update the container or contained item to remove the linking
      let itemToDelete = this.actor.items.find(item => item._id == li.dataset.itemId)

      // Logic for removing container linking if deleted item is the container
      if (itemToDelete.type == 'container') {
        // resets contained items status and then sets contained_items array to empty
        itemToDelete.system.contained_items.forEach(item => {

          let sourceItem = this.actor.items.find(i => i._id == item._id) 
          sourceItem.update({
            'system.containerStats.container_id': "",
            'system.containerStats.container_name': "",
            'system.containerStats.contained': false
          })
        })

        itemToDelete.update({'system.contained_items': []})

      }

      // Logic for removing container linking if deleted item is in a container
      if (itemToDelete.system.isPhysicalObject && itemToDelete.type != 'container' && itemToDelete.system.containerStats.contained) {
        let containerObject = this.actor.items.find(item => item._id == itemToDelete.system.containerStats.container_id)
        let indexToRemove = containerObject.system.contained_items.indexOf(containerObject.system.contained_items.find(i => i._id == itemToDelete._id))
        containerObject.system.contained_items.splice(indexToRemove, 1)
        containerObject.update({'system.contained_items': containerObject.system.contained_items})

        itemToDelete.update({
          'system.containerStats.container_id': "",
          'system.containerStats.container_name': "",
          'system.containerStats.contained': false
        })
      }

      this.actor.deleteEmbeddedDocuments("Item", [li.dataset.itemId]);
    });

  }

  /**
   * Handle clickable rolls.
   * @param event   The originating click event
   * @private
   */

  _duplicateItem(item) {
    let d = new Dialog({
      title: 'Duplicate Item',
      content: `<div style="padding: 10px; display: flex; flex-direction: row; align-items: center; justify-content: center;">
                    <div>Duplicate Item?</div>
                </div>`,
      buttons: {
        one: {
          label: 'Annuler',
          callback: html => console.log("Cancelled")
        },
        two: {
          label: 'Duplicate',
          callback: async (html) => {
            let newItem = await this.actor.createEmbeddedDocuments('Item', [item.toObject()])
            await newItem[0].sheet.render(true)
          }
        }
      },
      default: 'two',
      close: html => console.log()
    })

    d.render(true)
  }

  async _onSetBaseCharacteristics(event) {
      event.preventDefault()
      const forBonusArray = [];
      const endBonusArray = [];
      const agiBonusArray = [];
      const intBonusArray = [];
      // Willpower is set as wpC (instead of just 'wp' because the item value only contains 2 initial letters vs. 3 for all others... an inconsistency that is easier to resolve this way)
      const wpCBonusArray = [];
      const prcBonusArray = [];
      const prsBonusArray = [];
      const lckBonusArray = [];
      const sagBonusArray = [];
      const eloBonusArray = [];

      const bonusItems = this.actor.items.filter(item => item.system.hasOwnProperty("characteristicBonus"));

      for (let item of bonusItems) {
        for (let key in item.system.characteristicBonus) {
            let itemBonus = item.system.characteristicBonus[key]
            if (itemBonus !== 0) {
              let itemButton = `<button style="width: auto;" onclick="getItem(this.id, this.dataset.actor)" id="${item.id}" data-actor="${item.actor.id}">${item.name} ${itemBonus >= 0 ? `+${itemBonus}` : itemBonus}</button>`
              let bonusName = eval([...key].splice(0, 3).join('') + 'BonusArray')
              bonusName.push(itemButton)
            }
        }
      }

      let d = new Dialog({
        title: "Point de caractéristiques",
        content: `<form>
                    <script>
                      function getItem(itemID, actorID) {
                          let actor = game.actors.find(actor => actor.id === actorID)
                          let tokenActor = game.scenes.find(scene => scene.active === true).tokens.find(token => token.data.actorId === actorID)

                          if (actor.data.token.actorLink) {
                            let actorBonusItems = actor.items.filter(item => item.system.hasOwnProperty('characteristicBonus'))
                            let item = actorBonusItems.find(i => i.id === itemID)
                            item.sheet.render(true)
                          }
                          else {
                            let tokenBonusItems = tokenActor._actor.items.filter(item => item.system.hasOwnProperty('characteristicBonus'))
                            let item = tokenBonusItems.find(i => i.id === itemID)
                            item.sheet.render(true)
                          }
                        }
                    </script>

                    <h2>Fixer vos points de caractéristiques.</h2>

                    <div style="border: inset; margin-bottom: 10px; padding: 5px;">
                    <i>Utiliser ce menu pour fixer la valeur de vos points de caractéristiques.
                    Valeur maximum 300. 
                    10 minimum et 80 maximum. 
                    </i>
                    </div>

                    <div style="margin-bottom: 10px;">
                      <label><b>Points Total: </b></label>
                      <label>
                      ${this.actor.system.characteristics.for.base +
                      this.actor.system.characteristics.int.base +
                      this.actor.system.characteristics.elo.base +
                      this.actor.system.characteristics.prc.base +
                      this.actor.system.characteristics.sag.base + 
                      this.actor.system.characteristics.agi.base }
                      </label>
                      <table style="table-layout: fixed; text-align: center;">
                        <tr>
                          <th>FOR</th>
                          <th>INT</th>
                          <th>ELO</th>
                          <th>PRC</th>
                          <th>SAG</th>
                          <th>AGI</th>
                        </tr>
                        <tr>
                          <td><input type="number" id="forInput" value="${this.actor.system.characteristics.for.base}"></td>
                          <td><input type="number" id="intInput" value="${this.actor.system.characteristics.int.base}"></td>
                          <td><input type="number" id="eloInput" value="${this.actor.system.characteristics.elo.base}"></td>
                          <td><input type="number" id="prcInput" value="${this.actor.system.characteristics.prc.base}"></td>
                          <td><input type="number" id="sagInput" value="${this.actor.system.characteristics.sag.base}"></td>
                          <td><input type="number" id="agiInput" value="${this.actor.system.characteristics.agi.base}"></td>
                        </tr>
                      </table>
                    </div>

                    <div class="modifierBox" style="display:none;">
                      <h2>Modifieurs FOR</h2>
                      <span style="font-size: small">${forBonusArray.join('')}</span>
                    </div>

                    <div class="modifierBox" style="display:none;">
                      <h2>Modifieurs INT</h2>
                      <span style="font-size: small">${intBonusArray.join('')}</span>
                    </div>

                    <div class="modifierBox" style="display:none;">
                      <h2>Modifieurs ELO</h2>
                      <span style="font-size: small">${prsBonusArray.join('')}</span>
                    </div>

                    <div class="modifierBox" style="display:none;">
                      <h2>Modifieurs PER</h2>
                      <span style="font-size: small">${prcBonusArray.join('')}</span>
                    </div>

                    <div class="modifierBox" style="display:none;">
                      <h2>Modifieurs SAG</h2>
                      <span style="font-size: small">${sagBonusArray.join('')}</span>
                    </div>

                    <div class="modifierBox" style="display:none;">
                      <h2>Modifieurs AGI</h2>
                      <span style="font-size: small">${agiBonusArray.join('')}</span>
                    </div>

                    <div class="skillContainer">
                    <h2 class="headerWithButton">
                        <label>Compétences</label>
                    </h2>
                    <table>
                      <tbody>
                        <tr>
                          <div class="combat-tab-primary-header">
                            <th>
                              <div class="resistance-container">
                                  <h2>FORCE</h2>
                                  <div class="resistance-grid">
                                      <div class="flex-container">
                                          <div>Parade</div>
                                          <div><input class="competences" type="number" id="paradeInput" value="${this.actor.system.characteristics.for.competences.parade.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Lancer</div>
                                          <div><input class="competences" type="number" id="lancerInput" value="${this.actor.system.characteristics.for.competences.lancer.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Mains nues</div>
                                          <div><input class="competences" type="number" id="mains_nuesInput" value="${this.actor.system.characteristics.for.competences.mains_nues.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Armes moyennes et lourdes</div>
                                          <div><input class="competences" type="number" id="armes_moyennes_lourdesInput" value="${this.actor.system.characteristics.for.competences.armes_moyennes_lourdes.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Endurance</div>
                                          <div><input class="competences" type="number" id="enduranceInput" value="${this.actor.system.characteristics.for.competences.endurance.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Intimidation</div>
                                          <div><input class="competences" type="number" id="intimidationInput" value="${this.actor.system.characteristics.for.competences.intimidation.base}"></div>
                                      </div>
                                  </div>
                              </div>
                            </th>
                            <th>
                              <div class="status-controls-container">
                                  <h2>INTELLIGENCE</h2>
                                  <div class="resistance-grid">
                                      <div class="flex-container">
                                          <div>Histoire</div>
                                          <div><input class="competences" type="number" id="histoireInput" value="${this.actor.system.characteristics.int.competences.histoire.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Langues</div>
                                          <div><input class="competences" type="number" id="languesInput" value="${this.actor.system.characteristics.int.competences.langues.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Alchimie</div>
                                          <div><input class="competences" type="number" id="alchimieInput" value="${this.actor.system.characteristics.int.competences.alchimie.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Médecine</div>
                                          <div><input class="competences" type="number" id="medecineInput" value="${this.actor.system.characteristics.int.competences.medecine.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Arcanes</div>
                                          <div><input class="competences" type="number" id="arcanesInput" value="${this.actor.system.characteristics.int.competences.arcanes.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Nature</div>
                                          <div><input class="competences" type="number" id="natureInput" value="${this.actor.system.characteristics.int.competences.nature.base}"></div>
                                      </div>
                                  </div>
                              </div>
                            </th>
                          </div>
                        </tr>
                        <tr>
                          <div class="combat-tab-primary-header">
                            <th>
                              <div class="status-controls-container">
                                  <h2>ELOQUENCE</h2>
                                  <div class="resistance-grid">
                                      <div class="flex-container">
                                          <div>Charme</div>
                                          <div><input class="competences" type="number" id="charmeInput" value="${this.actor.system.characteristics.elo.competences.charme.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Spectacle</div>
                                          <div><input class="competences" type="number" id="spectacleInput" value="${this.actor.system.characteristics.elo.competences.spectacle.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Bluff</div>
                                          <div><input class="competences" type="number" id="bluffInput" value="${this.actor.system.characteristics.elo.competences.bluff.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Mensonge</div>
                                          <div><input class="competences" type="number" id="mensongeInput" value="${this.actor.system.characteristics.elo.competences.mensonge.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Négociation</div>
                                          <div><input class="competences" type="number" id="negociationInput" value="${this.actor.system.characteristics.elo.competences.negociation.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Troc</div>
                                          <div><input class="competences" type="number" id="trocInput" value="${this.actor.system.characteristics.elo.competences.troc.base}"></div>
                                      </div>
                                  </div>
                              </div>
                            </th>
                            <th>
                              <div class="status-controls-container">
                                  <h2>PERCEPTION</h2>
                                  <div class="resistance-grid">
                                      <div class="flex-container">
                                          <div>Reflexe</div>
                                          <div><input class="competences" type="number" id="reflexeInput" value="${this.actor.system.characteristics.prc.competences.reflexe.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Ecoute</div>
                                          <div><input class="competences" type="number" id="ecouteInput" value="${this.actor.system.characteristics.prc.competences.ecoute.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Détect Mag</div>
                                          <div><input class="competences" type="number" id="detectionMInput" value="${this.actor.system.characteristics.prc.competences.detectionM.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Repérage</div>
                                          <div><input class="competences" type="number" id="reperageInput" value="${this.actor.system.characteristics.prc.competences.reperage.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Discernemnt</div>
                                          <div><input class="competences" type="number" id="discernementInput" value="${this.actor.system.characteristics.prc.competences.discernement.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Pistage</div>
                                          <div><input class="competences" type="number" id="pistageInput" value="${this.actor.system.characteristics.prc.competences.pistage.base}"></div>
                                      </div>
                                  </div>
                              </div>
                            </th>
                          </div>
                        </tr>
                        <tr>
                          <div class="combat-tab-primary-header">
                            <th>
                              <div class="status-controls-container">
                                  <h2>SAGESSE</h2>
                                  <div class="resistance-grid">
                                      <div class="flex-container">
                                          <div>Concentrat°</div>
                                          <div><input class="competences" type="number" id="concentrationInput" value="${this.actor.system.characteristics.sag.competences.concentration.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Intuition</div>
                                          <div><input class="competences" type="number" id="intuitionInput" value="${this.actor.system.characteristics.sag.competences.intuition.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Survie</div>
                                          <div><input class="competences" type="number" id="survieInput" value="${this.actor.system.characteristics.sag.competences.survie.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Premiers soins</div>
                                          <div><input class="competences" type="number" id="premiers_soinsInput" value="${this.actor.system.characteristics.sag.competences.premiers_soins.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Intuit Mag</div>
                                          <div><input class="competences" type="number" id="intuitionMInput" value="${this.actor.system.characteristics.sag.competences.intuitionM.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Artisanats</div>
                                          <div><input class="competences" type="number" id="artisanatsInput" value="${this.actor.system.characteristics.sag.competences.artisanats.base}"></div>
                                      </div>
                                  </div>
                              </div>
                            </th>
                            <th>
                              <div class="status-controls-container">
                                  <h2>AGILITE</h2>
                                  <div class="resistance-grid">
                                      <div class="flex-container">
                                          <div>Esquive</div>
                                          <div><input class="competences" type="number" id="esquiveInput" value="${this.actor.system.characteristics.agi.competences.esquive.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Discretion</div>
                                          <div><input class="competences" type="number" id="discretionInput" value="${this.actor.system.characteristics.agi.competences.discretion.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Crochetage</div>
                                          <div><input class="competences" type="number" id="crochetageInput" value="${this.actor.system.characteristics.agi.competences.crochetage.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Initiative</div>
                                          <div><input class="competences" type="number" id="initiativeInput" value="${this.actor.system.characteristics.agi.competences.initiative.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Acrobatie</div>
                                          <div><input class="competences" type="number" id="acrobatieInput" value="${this.actor.system.characteristics.agi.competences.acrobatie.base}"></div>
                                      </div>
                                      <div class="flex-container">
                                          <div>Armes courtes et distances</div>
                                          <div><input class="competences" type="number" id="armes_courtes_distancesInput" value="${this.actor.system.characteristics.agi.competences.armes_courtes_distances.base}"></div>
                                      </div>
                                  </div>
                              </div>
                            </th>
                          </div>  
                        </tr>
                      </tbody>
                    </table>  
                </div>
                  </form>`,
      buttons: {
        one: {
          label: "Valider",
          callback: async (html) => {
            const forInput = parseInt(html.find('[id="forInput"]').val());
            const paradeInput = parseInt(html.find('[id="paradeInput"]').val());
            const lancerInput = parseInt(html.find('[id="lancerInput"]').val());
            const mains_nuesInput = parseInt(html.find('[id="mains_nuesInput"]').val());
            const armes_moyennes_lourdesInput = parseInt(html.find('[id="armes_moyennes_lourdesInput"]').val());
            const enduranceInput = parseInt(html.find('[id="enduranceInput"]').val());
            const intimidationInput = parseInt(html.find('[id="intimidationInput"]').val());
            
            const intInput = parseInt(html.find('[id="intInput"]').val());
            const histoireInput = parseInt(html.find('[id="histoireInput"]').val());
            const languesInput = parseInt(html.find('[id="languesInput"]').val());
            const alchimieInput = parseInt(html.find('[id="alchimieInput"]').val());
            const medecineInput = parseInt(html.find('[id="medecineInput"]').val());
            const arcanesInput = parseInt(html.find('[id="arcanesInput"]').val());
            const natureInput = parseInt(html.find('[id="natureInput"]').val());

            const eloInput = parseInt(html.find('[id="eloInput"]').val());
            const charmeInput = parseInt(html.find('[id="charmeInput"]').val());
            const spectacleInput = parseInt(html.find('[id="spectacleInput"]').val());
            const bluffInput = parseInt(html.find('[id="bluffInput"]').val());
            const mensongeInput = parseInt(html.find('[id="mensongeInput"]').val());
            const negociationInput = parseInt(html.find('[id="negociationInput"]').val());
            const trocInput = parseInt(html.find('[id="trocInput"]').val());

            const prcInput = parseInt(html.find('[id="prcInput"]').val());
            const reflexeInput = parseInt(html.find('[id="reflexeInput"]').val());
            const ecouteInput = parseInt(html.find('[id="ecouteInput"]').val());
            const detectionMInput = parseInt(html.find('[id="detectionMInput"]').val());
            const reperageInput = parseInt(html.find('[id="reperageInput"]').val());
            const discernementInput = parseInt(html.find('[id="discernementInput"]').val());
            const pistageInput = parseInt(html.find('[id="pistageInput"]').val());
            
            const sagInput = parseInt(html.find('[id="sagInput"]').val());
            const concentrationInput = parseInt(html.find('[id="concentrationInput"]').val());
            const intuitionInput = parseInt(html.find('[id="intuitionInput"]').val());
            const survieInput = parseInt(html.find('[id="survieInput"]').val());
            const premiers_soinsInput = parseInt(html.find('[id="premiers_soinsInput"]').val());
            const intuitionMInput = parseInt(html.find('[id="intuitionMInput"]').val());
            const artisanatsInput = parseInt(html.find('[id="artisanatsInput"]').val());
            
            const agiInput = parseInt(html.find('[id="agiInput"]').val());
            const esquiveInput = parseInt(html.find('[id="esquiveInput"]').val());
            const discretionInput = parseInt(html.find('[id="discretionInput"]').val());
            const crochetageInput = parseInt(html.find('[id="crochetageInput"]').val());
            const initiativeInput = parseInt(html.find('[id="initiativeInput"]').val());
            const acrobatieInput = parseInt(html.find('[id="acrobatieInput"]').val());
            const armes_courtes_distancesInput = parseInt(html.find('[id="armes_courtes_distancesInput"]').val());

            //Shortcut for characteristics
            const chaPath = this.actor.system.characteristics;

            //Assign values to characteristics
            //FORCE
            chaPath.for.base = forInput;
            chaPath.for.total = forInput + chaPath.for.bonus;
            await this.actor.update({
              "data.characteristics.for.base" : forInput,
              "data.characteristics.for.total": chaPath.for.total
            });
            chaPath.for.competences.parade.base = paradeInput;
            chaPath.for.competences.parade.total = paradeInput + chaPath.for.competences.parade.bonus;
            await this.actor.update({
              "data.characteristics.for.competences.parade.base" : paradeInput,
              "data.characteristics.for.competences.parade.total": chaPath.for.competences.parade.total
            });
            chaPath.for.competences.lancer.base = lancerInput;
            chaPath.for.competences.lancer.total = lancerInput + chaPath.for.competences.lancer.bonus;
            await this.actor.update({
              "data.characteristics.for.competences.lancer.base" : lancerInput,
              "data.characteristics.for.competences.lancer.total": chaPath.for.competences.lancer.total
            });
            chaPath.for.competences.mains_nues.base = mains_nuesInput;
            chaPath.for.competences.mains_nues.total = mains_nuesInput + chaPath.for.competences.mains_nues.bonus;
            await this.actor.update({
              "data.characteristics.for.competences.mains_nues.base" : mains_nuesInput,
              "data.characteristics.for.competences.mains_nues.total": chaPath.for.competences.mains_nues.total
            });
            chaPath.for.competences.armes_moyennes_lourdes.base = armes_moyennes_lourdesInput;
            chaPath.for.competences.armes_moyennes_lourdes.total = armes_moyennes_lourdesInput + chaPath.for.competences.armes_moyennes_lourdes.bonus;
            await this.actor.update({
              "data.characteristics.for.competences.armes_moyennes_lourdes.base" : armes_moyennes_lourdesInput,
              "data.characteristics.for.competences.armes_moyennes_lourdes.total": chaPath.for.competences.armes_moyennes_lourdes.total
            });
            chaPath.for.competences.endurance.base = enduranceInput;
            chaPath.for.competences.endurance.total = enduranceInput + chaPath.for.competences.endurance.bonus;
            await this.actor.update({
              "data.characteristics.for.competences.endurance.base" : enduranceInput,
              "data.characteristics.for.competences.endurance.total": chaPath.for.competences.endurance.total
            });
            chaPath.for.competences.intimidation.base = intimidationInput;
            chaPath.for.competences.intimidation.total = intimidationInput + chaPath.for.competences.intimidation.bonus;
            await this.actor.update({
              "data.characteristics.for.competences.intimidation.base" : intimidationInput,
              "data.characteristics.for.competences.intimidation.total": chaPath.for.competences.intimidation.total
            });

            //INTELLIGENCE
            chaPath.int.base = intInput;
            chaPath.int.total = intInput + chaPath.int.bonus;
            await this.actor.update({
              "data.characteristics.int.base" : intInput,
              "data.characteristics.int.total": chaPath.int.total
            });
            chaPath.int.competences.histoire.base = histoireInput;
            chaPath.int.competences.histoire.total = histoireInput + chaPath.int.competences.histoire.bonus;
            await this.actor.update({
              "data.characteristics.int.competences.histoire.base" : histoireInput,
              "data.characteristics.int.competences.histoire.total": chaPath.int.competences.histoire.total
            });
            chaPath.int.competences.langues.base = languesInput;
            chaPath.int.competences.langues.total = languesInput + chaPath.int.competences.langues.bonus;
            await this.actor.update({
              "data.characteristics.int.competences.langues.base" : languesInput,
              "data.characteristics.int.competences.langues.total": chaPath.int.competences.langues.total
            });
            chaPath.int.competences.alchimie.base = alchimieInput;
            chaPath.int.competences.alchimie.total = alchimieInput + chaPath.int.competences.alchimie.bonus;
            await this.actor.update({
              "data.characteristics.int.competences.alchimie.base" : alchimieInput,
              "data.characteristics.int.competences.alchimie.total": chaPath.int.competences.alchimie.total
            });
            chaPath.int.competences.medecine.base = medecineInput;
            chaPath.int.competences.medecine.total = medecineInput + chaPath.int.competences.medecine.bonus;
            await this.actor.update({
              "data.characteristics.int.competences.medecine.base" : medecineInput,
              "data.characteristics.int.competences.medecine.total": chaPath.int.competences.medecine.total
            });
            chaPath.int.competences.arcanes.base = arcanesInput;
            chaPath.int.competences.arcanes.total = arcanesInput + chaPath.int.competences.arcanes.bonus;
            await this.actor.update({
              "data.characteristics.int.competences.arcanes.base" : arcanesInput,
              "data.characteristics.int.competences.arcanes.total": chaPath.int.competences.arcanes.total
            });
            chaPath.int.competences.nature.base = natureInput;
            chaPath.int.competences.nature.total = natureInput + chaPath.int.competences.nature.bonus;
            await this.actor.update({
              "data.characteristics.int.competences.nature.base" : natureInput,
              "data.characteristics.int.competences.nature.total": chaPath.int.competences.nature.total
            });

            //ELOQUENCE
            chaPath.elo.base = eloInput;
            chaPath.elo.total = eloInput + chaPath.elo.bonus;
            await this.actor.update({
              "data.characteristics.elo.base" : eloInput,
              "data.characteristics.elo.total": chaPath.elo.total
            });
            chaPath.elo.competences.charme.base = charmeInput;
            chaPath.elo.competences.charme.total = charmeInput + chaPath.elo.competences.charme.bonus;
            await this.actor.update({
              "data.characteristics.elo.competences.charme.base" : charmeInput,
              "data.characteristics.elo.competences.charme.total": chaPath.elo.competences.charme.total
            });
            chaPath.elo.competences.spectacle.base = spectacleInput;
            chaPath.elo.competences.spectacle.total = spectacleInput + chaPath.elo.competences.spectacle.bonus;
            await this.actor.update({
              "data.characteristics.elo.competences.spectacle.base" : spectacleInput,
              "data.characteristics.elo.competences.spectacle.total": chaPath.elo.competences.spectacle.total
            });
            chaPath.elo.competences.bluff.base = bluffInput;
            chaPath.elo.competences.bluff.total = bluffInput + chaPath.elo.competences.bluff.bonus;
            await this.actor.update({
              "data.characteristics.elo.competences.bluff.base" : bluffInput,
              "data.characteristics.elo.competences.bluff.total": chaPath.elo.competences.bluff.total
            });
            chaPath.elo.competences.mensonge.base = mensongeInput;
            chaPath.elo.competences.mensonge.total = mensongeInput + chaPath.elo.competences.mensonge.bonus;
            await this.actor.update({
              "data.characteristics.elo.competences.mensonge.base" : mensongeInput,
              "data.characteristics.elo.competences.mensonge.total": chaPath.elo.competences.mensonge.total
            });
            chaPath.elo.competences.negociation.base = negociationInput;
            chaPath.elo.competences.negociation.total = negociationInput + chaPath.elo.competences.negociation.bonus;
            await this.actor.update({
              "data.characteristics.elo.competences.negociation.base" : negociationInput,
              "data.characteristics.elo.competences.negociation.total": chaPath.elo.competences.negociation.total
            });
            chaPath.elo.competences.troc.base = trocInput;
            chaPath.elo.competences.troc.total = trocInput + chaPath.elo.competences.troc.bonus;
            await this.actor.update({
              "data.characteristics.elo.competences.troc.base" : trocInput,
              "data.characteristics.elo.competences.troc.total": chaPath.elo.competences.troc.total
            });

            //PERCEPTION
            chaPath.prc.base = prcInput;
            chaPath.prc.total = prcInput + chaPath.prc.bonus;
            await this.actor.update({
              "data.characteristics.prc.base" : prcInput,
              "data.characteristics.prc.total": chaPath.prc.total
            });
            chaPath.prc.competences.reflexe.base = reflexeInput;
            chaPath.prc.competences.reflexe.total = reflexeInput + chaPath.prc.competences.reflexe.bonus;
            await this.actor.update({
              "data.characteristics.prc.competences.reflexe.base" : reflexeInput,
              "data.characteristics.prc.competences.reflexe.total": chaPath.prc.competences.reflexe.total
            });
            chaPath.prc.competences.ecoute.base = ecouteInput;
            chaPath.prc.competences.ecoute.total = ecouteInput + chaPath.prc.competences.ecoute.bonus;
            await this.actor.update({
              "data.characteristics.prc.competences.ecoute.base" : ecouteInput,
              "data.characteristics.prc.competences.ecoute.total": chaPath.prc.competences.ecoute.total
            });
            chaPath.prc.competences.detectionM.base = detectionMInput;
            chaPath.prc.competences.detectionM.total = detectionMInput + chaPath.prc.competences.detectionM.bonus;
            await this.actor.update({
              "data.characteristics.prc.competences.detectionM.base" : detectionMInput,
              "data.characteristics.prc.competences.detectionM.total": chaPath.prc.competences.detectionM.total
            });
            chaPath.prc.competences.reperage.base = reperageInput;
            chaPath.prc.competences.reperage.total = reperageInput + chaPath.prc.competences.reperage.bonus;
            await this.actor.update({
              "data.characteristics.prc.competences.reperage.base" : reperageInput,
              "data.characteristics.prc.competences.reperage.total": chaPath.prc.competences.reperage.total
            });
            chaPath.prc.competences.discernement.base = discernementInput;
            chaPath.prc.competences.discernement.total = discernementInput + chaPath.prc.competences.discernement.bonus;
            await this.actor.update({
              "data.characteristics.prc.competences.discernement.base" : discernementInput,
              "data.characteristics.prc.competences.discernement.total": chaPath.prc.competences.discernement.total
            });
            chaPath.prc.competences.pistage.base = pistageInput;
            chaPath.prc.competences.pistage.total = pistageInput + chaPath.prc.competences.pistage.bonus;
            await this.actor.update({
              "data.characteristics.prc.competences.pistage.base" : pistageInput,
              "data.characteristics.prc.competences.pistage.total": chaPath.prc.competences.pistage.total
            });

            //SAGESSE
            chaPath.sag.base = sagInput;
            chaPath.sag.total = sagInput + chaPath.sag.bonus;
            await this.actor.update({
              "data.characteristics.sag.base" : sagInput,
              "data.characteristics.sag.total": chaPath.sag.total
            });
            chaPath.sag.competences.concentration.base = concentrationInput;
            chaPath.sag.competences.concentration.total = concentrationInput + chaPath.sag.competences.concentration.bonus;
            await this.actor.update({
              "data.characteristics.sag.competences.concentration.base" : concentrationInput,
              "data.characteristics.sag.competences.concentration.total": chaPath.sag.competences.concentration.total
            });
            chaPath.sag.competences.intuition.base = intuitionInput;
            chaPath.sag.competences.intuition.total = intuitionInput + chaPath.sag.competences.intuition.bonus;
            await this.actor.update({
              "data.characteristics.sag.competences.intuition.base" : intuitionInput,
              "data.characteristics.sag.competences.intuition.total": chaPath.sag.competences.intuition.total
            });
            chaPath.sag.competences.survie.base = survieInput;
            chaPath.sag.competences.survie.total = survieInput + chaPath.sag.competences.survie.bonus;
            await this.actor.update({
              "data.characteristics.sag.competences.survie.base" : survieInput,
              "data.characteristics.sag.competences.survie.total": chaPath.sag.competences.survie.total
            });
            chaPath.sag.competences.premiers_soins.base = premiers_soinsInput;
            chaPath.sag.competences.premiers_soins.total = premiers_soinsInput + chaPath.sag.competences.premiers_soins.bonus;
            await this.actor.update({
              "data.characteristics.sag.competences.premiers_soins.base" : premiers_soinsInput,
              "data.characteristics.sag.competences.premiers_soins.total": chaPath.sag.competences.premiers_soins.total
            });
            chaPath.sag.competences.intuitionM.base = intuitionMInput;
            chaPath.sag.competences.intuitionM.total = intuitionMInput + chaPath.sag.competences.intuitionM.bonus;
            await this.actor.update({
              "data.characteristics.sag.competences.intuitionM.base" : intuitionMInput,
              "data.characteristics.sag.competences.intuitionM.total": chaPath.sag.competences.intuitionM.total
            });
            chaPath.sag.competences.artisanats.base = artisanatsInput;
            chaPath.sag.competences.artisanats.total = artisanatsInput + chaPath.sag.competences.artisanats.bonus;
            await this.actor.update({
              "data.characteristics.sag.competences.artisanats.base" : artisanatsInput,
              "data.characteristics.sag.competences.artisanats.total": chaPath.sag.competences.artisanats.total
            });

            //AGILITE
            chaPath.agi.base = agiInput;
            chaPath.agi.total = agiInput + chaPath.agi.bonus;
            await this.actor.update({
              "data.characteristics.agi.base" : agiInput,
              "data.characteristics.agi.total": chaPath.agi.total
            });
            chaPath.agi.competences.esquive.base = esquiveInput;
            chaPath.agi.competences.esquive.total = esquiveInput + chaPath.agi.competences.esquive.bonus;
            await this.actor.update({
              "data.characteristics.agi.competences.esquive.base" : esquiveInput,
              "data.characteristics.agi.competences.esquive.total": chaPath.agi.competences.esquive.total
            });
            chaPath.agi.competences.discretion.base = discretionInput;
            chaPath.agi.competences.discretion.total = discretionInput + chaPath.agi.competences.discretion.bonus;
            await this.actor.update({
              "data.characteristics.agi.competences.discretion.base" : discretionInput,
              "data.characteristics.agi.competences.discretion.total": chaPath.agi.competences.discretion.total
            });
            chaPath.agi.competences.crochetage.base = crochetageInput;
            chaPath.agi.competences.crochetage.total = crochetageInput + chaPath.agi.competences.crochetage.bonus;
            await this.actor.update({
              "data.characteristics.agi.competences.crochetage.base" : crochetageInput,
              "data.characteristics.agi.competences.crochetage.total": chaPath.agi.competences.crochetage.total
            });
            chaPath.agi.competences.initiative.base = initiativeInput;
            chaPath.agi.competences.initiative.total = initiativeInput + chaPath.agi.competences.initiative.bonus;
            await this.actor.update({
              "data.characteristics.agi.competences.initiative.base" : initiativeInput,
              "data.characteristics.agi.competences.initiative.total": chaPath.agi.competences.initiative.total
            });
            chaPath.agi.competences.acrobatie.base = acrobatieInput;
            chaPath.agi.competences.acrobatie.total = acrobatieInput + chaPath.agi.competences.acrobatie.bonus;
            await this.actor.update({
              "data.characteristics.agi.competences.acrobatie.base" : acrobatieInput,
              "data.characteristics.agi.competences.acrobatie.total": chaPath.agi.competences.acrobatie.total
            });
            chaPath.agi.competences.armes_courtes_distances.base = armes_courtes_distancesInput;
            chaPath.agi.competences.armes_courtes_distances.total = armes_courtes_distancesInput + chaPath.agi.competences.armes_courtes_distances.bonus;
            await this.actor.update({
              "data.characteristics.agi.competences.armes_courtes_distances.base" : armes_courtes_distancesInput,
              "data.characteristics.agi.competences.armes_courtes_distances.total": chaPath.agi.competences.armes_courtes_distances.total
            });

          }
        },
        two: {
          label: "Annuler",
          callback: async (html) => console.log("Cancelled")
        }
      },
      default: "one",
      close: async (html) => console.log()
    })
    d.render(true);
  }

  async _onClickCharacteristic(event) {
    event.preventDefault()
    const element = event.currentTarget
    const woundedValue = this.actor.system.characteristics[element.id].total
    const regularValue = this.actor.system.characteristics[element.id].total 
    let tags = []
    if (this.actor.system.wounded) {tags.push(`<span class="tag wound-tag">Wounded ${this.actor.system.woundPenalty}</span>`)}
    if (this.actor.system.carry_rating.penalty != 0) {tags.push(`<span class="tag enc-tag">Encumbered ${this.actor.system.carry_rating.penalty}</span>`)}

    // Dialog Menu
    let d = new Dialog({
      title: "Appliquer modificateur",
      content: `<form>
                  <div class="dialogForm">
                  <label><b>Modificateur ${element.getAttribute('name')} : </b></label><input placeholder="ex. -20, +10" id="playerInput" value="0" style=" text-align: center; width: 50%; border-style: groove; float: right;" type="text"></input></div>
                </form>`,
      buttons: {
        one: {
          label: "Roll!",
          callback: html => {
            const playerInput = parseInt(html.find('[id="playerInput"]').val());

    let contentString = "";
    let roll = new Roll("1d100");
    roll.roll({async:false});

      contentString = `<h2>${element.getAttribute('name')}</h2
      <p></p><b>Charactéristique perso: [[${regularValue + playerInput}]]</b> <p></p>
      <b>Résultat:  [[${roll.result}]]</b><p></p>
      <b>${roll.total<=(regularValue + playerInput) ? "<span style='color:green; font-size: 120%;'> <b>Réussite!</b></span>" : " <span style='color:rgb(168, 5, 5); font-size: 120%;'> <b>Echec!</b></span>"}`


    ChatMessage.create({
      async:false, 
      type: CONST.CHAT_MESSAGE_TYPES.ROLL, 
      user: game.user.id, 
      speaker: ChatMessage.getSpeaker(), 
      roll: roll,
      content: contentString,
      flavor: `<div class="tag-container">${tags.join('')}</div>`
    })

    }
  },
  two: {
    label: "Annuler",
    callback: html => console.log("Cancelled")
  }
  },
  default: "one",
  close: html => console.log()
  });
  d.render(true);
  }

  async _onSkillRoll(event) {
    event.preventDefault()
    const button = event.currentTarget;
    const li = button.closest(".item");
    const item = this.actor.items.get(li?.dataset.itemId);

    const woundedValue = item.system.value 
    const regularValue = item.system.value 
    let tags = []
    if (this.actor.system.wounded) {tags.push(`<span class="tag wound-tag">Wounded ${this.actor.system.woundPenalty}</span>`)}
    if (this.actor.system.carry_rating.penalty != 0) {tags.push(`<span class="tag enc-tag">Encumbered ${this.actor.system.carry_rating.penalty}</span>`)}

    // Skill Roll Dialog Menu
    let d = new Dialog({
      title: "Apply Roll Modifier",
      content: `<form>
                  <div class="dialogForm">
                  <label><b>${item.name} Modifier: </b></label><input placeholder="ex. -20, +10" id="playerInput" value="0" style=" text-align: center; width: 50%; border-style: groove; float: right;" type="text"></input></div>
                </form>`,
      buttons: {
        one: {
          label: "Roll!",
          callback: async (html) => {
            const playerInput = parseInt(html.find('[id="playerInput"]').val());
            let contentString = "";
            let roll = new Roll("1d100");
            roll.roll({async:false});

            contentString = `<h2><img src="${item.img}"</img>${item.name}</h2>
            <p></p><b>Charactéristique perso: [[${regularValue + playerInput}]]</b> <p></p>
            <b>Résultat: [[${roll.result}]]</b><p></p>
            <b>${roll.total<=(regularValue + playerInput) ? " <span style='color:green; font-size: 120%;'> <b>SUCCESS!</b></span>" : " <span style='color: rgb(168, 5, 5); font-size: 120%;'> <b>FAILURE!</b></span>"}`
          

          ChatMessage.create({
            async:false, 
            type: CONST.CHAT_MESSAGE_TYPES.ROLL, 
            user: game.user.id, 
            speaker: ChatMessage.getSpeaker(), 
            roll: roll,
            content: contentString,
            flavor: `<div class="tag-container">${tags.join('')}</div>`
          })
        }
      },
      two: {
        label: "Cancel",
        callback: html => console.log("Cancelled")
      }
      },
      default: "one",
      close: html => console.log()
      });
      d.render(true);
  }

  _onSpellRoll(event) {
    //Search for Talents that affect Spellcasting Costs
    let spellToCast

    if (event.currentTarget.closest('.item') != null || event.currentTarget.closest('.item') != undefined) {
      spellToCast = this.actor.items.find(spell => spell.id === event.currentTarget.closest('.item').dataset.itemId)
    }
    else {
      spellToCast = this.actor.getEmbeddedDocument('Item', this.actor.system.favorites[event.currentTarget.dataset.hotkey].id)
    }

    // const spellToCast = this.actor.items.find(spell => spell.id === event.currentTarget.closest('.item').dataset.itemId)
    const hasCreative = this.actor.items.find(i => i.type === "talent" && i.name === "Creative") ? true : false;
    const hasForceOfWill = this.actor.items.find(i => i.type === "talent" && i.name === "Force of Will") ? true : false;
    const hasMethodical = this.actor.items.find(i => i.type === "talent" && i.name === "Methodical") ? true : false;
    const hasOvercharge = this.actor.items.find(i => i.type === "talent" && i.name === "Overcharge") ? true : false;
    const hasMagickaCycling = this.actor.items.find(i => i.type === "talent" && i.name === "Magicka Cycling") ? true : false;

    //Add options in Dialog based on Talents and Traits
    let overchargeOption = ""
    let magickaCyclingOption = ""

    if (hasOvercharge){
        overchargeOption = `<tr>
                                <td><input type="checkbox" id="Overcharge"/></td>
                                <td><strong>Overcharge</strong></td>
                                <td>Roll damage twice and use the highest value (spell cost is doubled)</td>
                            </tr>`
    }

    if (hasMagickaCycling){
        magickaCyclingOption = `<tr>
                                    <td><input type="checkbox" id="MagickaCycling"/></td>
                                    <td><strong>Magicka Cycling</strong></td>
                                    <td>Double Restraint Value, but backfires on failure</td>
                                </tr>`
    }

    // If Description exists, put into the dialog for reference
    let spellDescriptionDiv = ''
    if (spellToCast.system.description != '' && spellToCast.system.description != undefined) {
      spellDescriptionDiv = `<div style="padding: 10px;">
                                  ${spellToCast.system.description}
                              </div>`
    }

      const m = new Dialog({
        title: "Préparation Sort/Compétence",
        content: `<form>
                    <div>

                        <div>
                            <h2 style="text-align: center; display: flex; flex-direction: row; align-items: center; justify-content: center; gap: 5px; font-size: xx-large;">
                                <img src="${spellToCast.img}" class="item-img" height=35 width=35>
                                <div>${spellToCast.name}</div>
                            </h2>

                            <table>
                                <thead>
                                    <tr>
                                      <th>Portée(m)</th>  
                                      <th>Rayon(m)</th>
                                    </tr>
                                </thead>
                                <tbody style="text-align: center;">
                                    <tr>
                                      <td>${spellToCast.system.portee}</td>  
                                      <td>${spellToCast.system.rayon}</td>  
                                    </tr>
                                </tbody>
                            </table>

                            ${spellDescriptionDiv}
                            <div class="dialogForm2">
                              <label><b>Modifieur de dégâts : </b></label><input placeholder="ex. -20, +10" id="playerInput" value="0" style=" text-align: center; width: 50%; border-style: groove; float: right;" type="text"></input>
                            </div>
                            <div class="dialogForm2">
                              <label><b>Modifieur dé+ : </b></label><input placeholder="ex. -20, +10" id="playerInputDe3" value="0" style=" text-align: center; width: 50%; border-style: groove; float: right;" type="text"></input>
                            </div>
                            <div class="dialogForm2">
                              <label><b>Modifieur de tour : </b></label><input placeholder="ex. -20, +10" id="playerInputTour" value="0" style=" text-align: center; width: 50%; border-style: groove; float: right;" type="text"></input>
                            </div>
                            <div hidden style="padding: 10px; margin-top: 10px; background: rgba(161, 149, 149, 0.486); border: black 1px; font-style: italic;">
                                Select one of the options below OR skip this to cast the spell without any modifications.
                            </div>
                        </div>

                        <table hidden>
                            <thead>
                                <tr>
                                    <th>Select</th>
                                    <th style="min-width: 120px;">Option</th>
                                    <th>Effect</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="checkbox" id="Restraint"/></td>
                                    <td><strong>Spell Restraint</strong></td>
                                    <td>Reduces cost of spell by WP Bonus</td>
                                </tr>
                                <tr>
                                    <td><input type="checkbox" id="Overload"/></td>
                                    <td><strong>Overload</strong></td>
                                    <td>Additional effects if not Restrained</td>
                                </tr>
                                ${magickaCyclingOption}
                                ${overchargeOption}
                            </tbody>
                        </table>

                    </div>
                  </form>`,
        buttons: {
            one: {
                label: "Lancer",
                callback: async (html) => {
                    let spellRestraint = 0;
                    let stackCostMod = 0;

                    //Assign Tags for Chat Output
                    const isRestrained = html.find(`[id="Restraint"]`)[0].checked;
                    const isOverloaded = html.find(`[id="Overload"]`)[0].checked;
                    const playerInput = html.find('[id="playerInput"]').val();
                    const playerInputDe3 = html.find('[id="playerInputDe3"]').val();
                    const playerInputTour = html.find('[id="playerInputTour"]').val();
                    let isMagickaCycled = "";
                    let isOvercharged = "";

                    if (hasMagickaCycling){
                        isMagickaCycled = html.find(`[id="MagickaCycling"]`)[0].checked;
                    }

                    if (hasOvercharge){
                        isOvercharged = html.find(`[id="Overcharge"]`)[0].checked;
                    }

                    const tags = [];


                    //Functions for Spell Modifiers
                    if (isRestrained){
                        let restraint = `<span style="border: none; border-radius: 30px; background-color: rgba(29, 97, 187, 0.80); color: white; text-align: center; font-size: xx-small; padding: 5px;">Restraint</span>`;
                        tags.push(restraint);

                        //Determine cost mod based on talents and other modifiers
                        if (hasCreative && spellToCast.system.spellType === "unconventional"){
                            stackCostMod = stackCostMod - 1;
                        } 

                        if (hasMethodical && spellToCast.system.spellType === "conventional"){
                            stackCostMod = stackCostMod - 1;
                        }
                        
                        if(hasForceOfWill){
                            stackCostMod = stackCostMod - 1;
                        }

                        spellRestraint = 0 - Math.floor(this.actor.system.characteristics.wp.total/10);
                    }

                    if (isOverloaded){
                        let overload = `<span style="border: none; border-radius: 30px; background-color: rgba(161, 2, 2, 0.80); color: white; text-align: center; font-size: xx-small; padding: 5px;">Overload</span>`;
                        tags.push(overload);
                    }

                    if (isMagickaCycled){
                        let cycled = `<span style="border: none; border-radius: 30px; background-color: rgba(126, 40, 224, 0.80); color: white; text-align: center; font-size: xx-small; padding: 5px;">Magicka Cycle</span>`;
                        tags.push(cycled);
                        spellRestraint = 0 - (2 * Math.floor(this.actor.system.characteristics.wp.total/10));
                    }


                    //If spell has damage value it outputs to Chat, otherwise no damage will be shown in Chat Output
                    const damageRoll = new Roll(spellToCast.system.damage+"+"+playerInput);
                    const de3Roll = new Roll(spellToCast.system.de3+"+"+playerInputDe3);
                    const tourRoll = new Roll(spellToCast.system.tour+"+"+playerInputTour);
                    let damageEntry = "";

                    if (spellToCast.system.damage != '' && spellToCast.system.damage != 0){
                        damageRoll.roll({async: false});
                        de3Roll.roll({async: false});
                        tourRoll.roll({async: false});
                        damageEntry = `<tr>
                                            <td style="font-weight: bold;">Dégâts</td>
                                            <td style="font-weight: bold; text-align: center;">[[${damageRoll.result}]]</td>
                                            <td style="text-align: center;">${damageRoll.formula}</td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Dé+</td>
                                            <td style="font-weight: bold; text-align: center;">[[${de3Roll.result}]]</td>
                                            <td style="text-align: center;">${de3Roll.formula}</td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold;">Tour</td>
                                            <td style="font-weight: bold; text-align: center;">[[${tourRoll.result}]]</td>
                                            <td style="text-align: center;">${tourRoll.formula}</td>
                                        </tr>`
                    }

                    

                    let displayCost = 0;
                    let actualCost = spellToCast.system.cost + spellRestraint + stackCostMod;

                    //Double Cost of Spell if Overcharge Talent is used
                    if (isOvercharged){
                        actualCost = actualCost * 2;
                        let overcharge = `<span style="border: none; border-radius: 30px; background-color: rgba(219, 135, 0, 0.8); color: white; text-align: center; font-size: xx-small; padding: 5px;">Overcharge</span>`;
                        tags.push(overcharge);
                    }

                    if (actualCost < 1){
                        displayCost = 1;
                    } else {
                        displayCost = actualCost;
                    }

                    // Stop The Function if the user does not have enough Magicka to Cast the Spell
                    if (game.settings.get("talthea-system", "automateMagicka")) {
                      if (displayCost > this.actor.system.magicka.value) {
                        return ui.notifications.info(`You do not have enough Magicka to cast this spell: Cost: ${spellToCast.system.cost} || Restraint: ${spellRestraint} || Other: ${stackCostMod}`)
                      }
                    }

                    let contentString = `<h2><img src=${spellToCast.img}></im>${spellToCast.name}</h2>
                                            <table>
                                                <thead>
                                                    <tr>
                                                        <th style="min-width: 80px;">Type</th>
                                                        <th style="min-width: 80px; text-align: center;">Resultat</th>
                                                        <th style="min-width: 80px; text-align: center;">Détail</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    ${damageEntry}
                                                </tbody>
                                            </table>`
                                            
                    damageRoll.toMessage({
                        user: game.user.id,
                        speaker: ChatMessage.getSpeaker(),
                        type: CONST.CHAT_MESSAGE_TYPES.ROLL,
                        flavor: tags.join(""),
                        content: contentString
                    })

                    // If Automate Magicka Setting is on, reduce the character's magicka by the calculated output cost
                    if (game.settings.get("talthea-system", "automateMagicka")) {this.actor.update({'data.magicka.value': this.actor.system.magicka.value - displayCost})}
                }
            },
            two: {
                label: "Annuler",
                callback: html => console.log("Cancelled")
            }
        },
        default: "one",
        close: html => console.log()
      });

    m.position.width = 450;
    m.render(true);
  }

  async _onCombatRoll(event) {
    event.preventDefault()
    let button = $(event.currentTarget);
    const li = button.parents(".item");
    const item = this.actor.getEmbeddedDocument("Item", li.data("itemId"));
    const woundedValue = item.system.value 
    const regularValue = item.system.value 
    let tags = []
    if (this.actor.system.wounded) {tags.push(`<span class="tag wound-tag">Wounded ${this.actor.system.woundPenalty}</span>`)}
    if (this.actor.system.carry_rating.penalty != 0) {tags.push(`<span class="tag enc-tag">Encumbered ${this.actor.system.carry_rating.penalty}</span>`)}

    let d = new Dialog({
      title: "Apply Roll Modifier",
      content: `<form>
                  <div class="dialogForm">
                  <label><b>${item.name} Modifier: </b></label><input placeholder="ex. -20, +10" id="playerInput" value="0" style=" text-align: center; width: 50%; border-style: groove; float: right;" type="text"></input></div>
                </form>`,
      buttons: {
        one: {
          label: "Roll!",
          callback: async (html) => {
            const playerInput = parseInt(html.find('[id="playerInput"]').val());

          let roll = new Roll("1d100");
          roll.roll({async:false});
          let contentString = "";
          

            contentString = `<h2><img src="${item.img}"</img>${item.name}</h2>
            <p></p><b>Charactéristique perso: [[${regularValue} + ${playerInput}]]</b> <p></p>
            <b>Résultat: [[${roll.result}]]</b><p></p>
            <b>${roll.total<=(regularValue + playerInput) ? " <span style='color:green; font-size: 120%;'> <b>SUCCESS!</b></span>" : " <span style='color: rgb(168, 5, 5); font-size: 120%;'> <b>FAILURE!</b></span>"}`
          

          ChatMessage.create({
            async:false, 
            type: CONST.CHAT_MESSAGE_TYPES.ROLL, 
            user: game.user.id, 
            speaker: ChatMessage.getSpeaker(), 
            roll: roll,
            content: contentString,
            flavor: `<div class="tag-container">${tags.join('')}</div>`
          })

        }
      },
      two: {
        label: "Cancel",
        callback: html => console.log("Cancelled")
      }
      },
      default: "one",
      close: html => console.log()
      });
      d.render(true);
  }

  async _onResistanceRoll(event) {
    event.preventDefault()
    const element = event.currentTarget

    let d = new Dialog({
      title: "Apply Roll Modifier",
      content: `<form>
                  <div class="dialogForm">
                  <label><b>${element.name} Resistance Modifier: </b></label><input placeholder="ex. -20, +10" id="playerInput" value="0" style=" text-align: center; width: 50%; border-style: groove; float: right;" type="text"></input></div>
                </form>`,
      buttons: {
        one: {
          label: "Roll!",
          callback: async (html) => {
            const playerInput = parseInt(html.find('[id="playerInput"]').val());

          let roll = new Roll("1d100");
          roll.roll({async:false});
          let contentString = "";


            contentString = `<h2>${element.name} Resistance</h2>
            <p></p><b>Charactéristique perso: [[${this.actor.system.resistance[element.id]} + ${playerInput}]]</b> <p></p>
            <b>Résultat: [[${roll.result}]]</b><p></p>
            <b>${roll.total<=(this.actor.system.resistance[element.id] + playerInput) ? " <span style='color:green; font-size: 120%;'> <b>SUCCESS!</b></span>" : " <span style='color: rgb(168, 5, 5); font-size: 120%;'> <b>FAILURE!</b></span>"}`
          
          await roll.toMessage({
            async: false,
            user: game.user.id,
            speaker: ChatMessage.getSpeaker(),
            content: contentString
          })
        }
      },
      two: {
        label: "Cancel",
        callback: html => console.log("Cancelled")
      }
      },
      default: "one",
      close: html => console.log()
      });
      d.render(true);

  }

  _onDamageRoll(event) {
    event.preventDefault()
    let itemElement = event.currentTarget.closest('.item')
    let shortcutWeapon = this.actor.getEmbeddedDocument("Item", itemElement.dataset.itemId)

    let hit_loc = ""
    let hit = new Roll("1d10")
    hit.roll({async: false})

    switch (hit.result) {
      case "1":
        hit_loc = "Body"
        break

      case "2":
        hit_loc = "Body"
        break

      case "3":
        hit_loc = "Body"
        break

      case "4":
        hit_loc = "Body"
        break

      case "5":
        hit_loc = "Body"
        break

      case "6":
        hit_loc = "Right Leg"
        break

      case "7":
        hit_loc = "Left Leg"
        break
      
      case "8":
        hit_loc = "Right Arm"
        break

      case "9":
        hit_loc = "Left Arm"
        break

      case "10":
        hit_loc = "Head"
        break
    }

    let damageString
    shortcutWeapon.system.weapon2H ? damageString = shortcutWeapon.system.damage2 : damageString = shortcutWeapon.system.damage
    let weaponRoll = new Roll(damageString)
    let d3Roll = new Roll(shortcutWeapon.system.de3)
    let tourRoll = new Roll(shortcutWeapon.system.tour)
    weaponRoll.roll({async: false})
    d3Roll.roll({async: false})
    tourRoll.roll({async: false})
    
    // Superior Weapon Roll
    let supRollTag = ``
    let superiorRoll = new Roll(damageString)
    superiorRoll.roll({async: false})

    if (shortcutWeapon.system.superior) {
      supRollTag = `[[${superiorRoll.result}]]`
    }

    let contentString = `<div>
                              <h2>
                                  <img src="${shortcutWeapon.img}">
                                  <div>${shortcutWeapon.name}</div>
                              </h2>

                              <table>
                                  <thead>
                                      <tr>
                                          <th>Damage</th>
                                          <th class="tableCenterText">Resultat</th>
                                          <th class="tableCenterText">Detail</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <tr>
                                          <td class="tableAttribute">Damage</td>
                                          <td class="tableCenterText">[[${weaponRoll.result}]] ${supRollTag}</td>
                                          <td class="tableCenterText">${damageString}</td>
                                      </tr>
                                      <tr>
                                          <td class="tableAttribute">Dé+</td>
                                          <td class="tableCenterText">[[${d3Roll.result}]] ${supRollTag}</td>
                                          <td class="tableCenterText">${shortcutWeapon.system.de3}</td>
                                      </tr>
                                      <tr>
                                          <td class="tableAttribute">Tour</td>
                                          <td class="tableCenterText">[[${tourRoll.result}]] ${supRollTag}</td>
                                          <td class="tableCenterText">${shortcutWeapon.system.tour}</td>
                                      </tr>
                                  </tbody>
                              </table>
                          <div>`


    // tags for flavor on chat message
    let tags = [];

    if (shortcutWeapon.system.superior){
        let tagEntry = `<span style="border: none; border-radius: 30px; background-color: rgba(29, 97, 187, 0.80); color: white; text-align: center; font-size: xx-small; padding: 5px;" title="Damage was rolled twice and output was highest of the two">Superior</span>`;
        tags.push(tagEntry);
    }

    ChatMessage.create({
      type: CONST.CHAT_MESSAGE_TYPES.ROLL,
      user: game.user.id,
      speaker: ChatMessage.getSpeaker(),
      flavor: tags.join(''),
      content: contentString,
      roll: weaponRoll,
      roll: d3Roll,
      roll: tourRoll
    })
  }
  _onDamageRollAccessoire(event) {
    event.preventDefault()
    let itemElement = event.currentTarget.closest('.item')
    let shortcutAccessoire = this.actor.getEmbeddedDocument("Item", itemElement.dataset.itemId)

    let hit_loc = ""
    let hit = new Roll("1d10")
    hit.roll({async: false})

    switch (hit.result) {
      case "1":
        hit_loc = "Body"
        break

      case "2":
        hit_loc = "Body"
        break

      case "3":
        hit_loc = "Body"
        break

      case "4":
        hit_loc = "Body"
        break

      case "5":
        hit_loc = "Body"
        break

      case "6":
        hit_loc = "Right Leg"
        break

      case "7":
        hit_loc = "Left Leg"
        break
      
      case "8":
        hit_loc = "Right Arm"
        break

      case "9":
        hit_loc = "Left Arm"
        break

      case "10":
        hit_loc = "Head"
        break
    }

    let damageString
    shortcutAccessoire.system.weapon2H ? damageString = shortcutAccessoire.system.damage2 : damageString = shortcutAccessoire.system.damage
    let accessoireRoll = new Roll(damageString)
    let d3Roll = new Roll(shortcutAccessoire.system.de3)
    let tourRoll = new Roll(shortcutAccessoire.system.tour)
    accessoireRoll.roll({async: false})
    d3Roll.roll({async: false})
    tourRoll.roll({async: false})
    
    // Superior Accessoire Roll
    let supRollTag = ``
    let superiorRoll = new Roll(damageString)
    superiorRoll.roll({async: false})

    if (shortcutAccessoire.system.superior) {
      supRollTag = `[[${superiorRoll.result}]]`
    }

    let contentString = `<div>
                              <h2>
                                  <img src="${shortcutAccessoire.img}">
                                  <div>${shortcutAccessoire.name}</div>
                              </h2>

                              <table>
                                  <thead>
                                      <tr>
                                          <th>Damage</th>
                                          <th class="tableCenterText">Resultat</th>
                                          <th class="tableCenterText">Detail</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <tr>
                                          <td class="tableAttribute">Damage</td>
                                          <td class="tableCenterText">[[${accessoireRoll.result}]] ${supRollTag}</td>
                                          <td class="tableCenterText">${damageString}</td>
                                      </tr>
                                      <tr>
                                          <td class="tableAttribute">Dé+</td>
                                          <td class="tableCenterText">[[${d3Roll.result}]] ${supRollTag}</td>
                                          <td class="tableCenterText">${shortcutAccessoire.system.de3}</td>
                                      </tr>
                                      <tr>
                                          <td class="tableAttribute">Tour</td>
                                          <td class="tableCenterText">[[${tourRoll.result}]] ${supRollTag}</td>
                                          <td class="tableCenterText">${shortcutAccessoire.system.tour}</td>
                                      </tr>
                                  </tbody>
                              </table>
                          <div>`


    // tags for flavor on chat message
    let tags = [];

    if (shortcutAccessoire.system.superior){
        let tagEntry = `<span style="border: none; border-radius: 30px; background-color: rgba(29, 97, 187, 0.80); color: white; text-align: center; font-size: xx-small; padding: 5px;" title="Damage was rolled twice and output was highest of the two">Superior</span>`;
        tags.push(tagEntry);
    }

    ChatMessage.create({
      type: CONST.CHAT_MESSAGE_TYPES.ROLL,
      user: game.user.id,
      speaker: ChatMessage.getSpeaker(),
      flavor: tags.join(''),
      content: contentString,
      roll: accessoireRoll,
      roll: d3Roll,
      roll: tourRoll
    })
  }
  async _onAmmoRoll(event) {
    event.preventDefault()
    let button = $(event.currentTarget);
    const li = button.parents(".item");
    const item = this.actor.getEmbeddedDocument("Item", li.data("itemId"));

    const contentString = `<h2><img src="${item.img}"</img>${item.name}</h2>
      <b>Damage Bonus:</b> ${item.system.damage}<p>
      <b>Qualities</b> ${item.system.qualities}`

      if (item.system.quantity > 0){
        await ChatMessage.create({
          user: game.user.id,
          speaker: ChatMessage.getSpeaker(),
          content: contentString
        })
      }

    item.system.quantity = item.system.quantity - 1;
    if (item.system.quantity < 0){
      item.system.quantity = 0;
      ui.notifications.info("Out of Ammunition!");
    }
      await item.update({"data.quantity" : item.system.quantity})
    }

  async _onToggle2H(event) {
    event.preventDefault()
    let toggle = $(event.currentTarget);
    const li = toggle.parents(".item");
    const item = this.actor.getEmbeddedDocument("Item", li.data("itemId"));

    if (item.system.weapon2H === false) {
      item.update({"system.weapon2H" : true})
    } else if (item.system.weapon2H === true) {
      item.update({"system.weapon2H" : false})
    }
  }

  async _onPlusQty(event) {
    event.preventDefault()
    let toggle = $(event.currentTarget);
    const li = toggle.parents(".item");
    const item = this.actor.getEmbeddedDocument("Item", li.data("itemId"));

    item.system.quantity = item.system.quantity + 1;

    await item.update({"system.quantity" : item.system.quantity})
  }

  async _onMinusQty(event) {
    event.preventDefault()
    let toggle = $(event.currentTarget);
    const li = toggle.parents(".item");
    const item = this.actor.getEmbeddedDocument("Item", li.data("itemId"));

    item.system.quantity = item.system.quantity - 1;
    if (item.system.quantity <= 0){
      item.system.quantity = 0;
      ui.notifications.info(`You have used your last ${item.name}!`);
    }

    await item.update({"system.quantity" : item.system.quantity})
  }

  async _onItemEquip(event) {
    let toggle = $(event.currentTarget);
    const li = toggle.closest(".item");
    const item = this.actor.getEmbeddedDocument("Item", li.data("itemId"));

    if (item.system.equipped === false) {
      item.update({"system.equipped" : true})
    } else if (item.system.equipped === true) {
      item.update({"system.equipped" : false})
    }
  }

  async _onItemCreate(event) {
    event.preventDefault()
    const element = event.currentTarget
    let itemData = [{name: element.id, type: element.id, 'data.baseCha': 'for'}]

    if (element.id === 'createSelect') {
      let d = new Dialog({
        title: "Création d'item",
        content: `<div style="padding: 10px 0;">
                      <h2>Selectionner le type</h2>
                      <label>Choisisser ce que vous voulez créer</label>
                  </div>`,

        buttons: {
          one: {
            label: "Item",
            callback: async html => {
                const itemData = [{name: 'item', type: 'item'}]
                let newItem = await this.actor.createEmbeddedDocuments("Item", itemData);
                await newItem[0].sheet.render(true)
            }
          },
          two: {
            label: "Accessoire",
            callback: async html => {
              const itemData = [{name: 'accessoire', type: 'accessoire'}]
              let newItem = await this.actor.createEmbeddedDocuments("Item", itemData);
              await newItem[0].sheet.render(true)
            }
          },
          three: {
            label: "Armure",
            callback: async html => {
                const itemData = [{name: 'armor', type: 'armor'}]
                let newItem = await this.actor.createEmbeddedDocuments("Item", itemData);
                await newItem[0].sheet.render(true)
            }
          },
          four: {
            label: "Arme",
            callback: async html => {
              const itemData = [{name: 'weapon', type: 'weapon'}]
              let newItem = await this.actor.createEmbeddedDocuments("Item", itemData);
              await newItem[0].sheet.render(true)
            }
          },
          five: {
            label: "Annuler",
            callback: html => console.log('Cancelled')
          }
        },
        default: "one",
        close: html => console.log()
      })

      d.render(true)
    }

    else {

      if (element.id === 'combatStyle') {
        itemData = [{
          name: element.id,
          type: element.id,
          'data.baseCha': this.actor.system.characteristics.for.total >= this.actor.system.characteristics.agi.total ? 'for' : 'agi'
        }]
      } 
      
      if (element.id === 'magicSkill') {
        itemData = [{
          name: element.id,
          type: element.id,
          'data.baseCha': this.actor.system.characteristics.int.total >= this.actor.system.characteristics.wp.total ? 'wp' : 'int'
        }]
      }


      let newItem = await this.actor.createEmbeddedDocuments("Item", itemData);
      await newItem[0].sheet.render(true)
    }
  }

  async _onTalentRoll(event) {
    event.preventDefault()
    let button = $(event.currentTarget);
    const li = button.parents(".item");
    const item = this.actor.getEmbeddedDocument("Item", li.data("itemId"));

    let contentString = `<h2><img src="${item.img}"</img>${item.name}</h2>
    <i><b>${item.type}</b></i><p>
      <i>${item.system.description}</i>`

    await ChatMessage.create({
      user: game.user.id,
      speaker: ChatMessage.getSpeaker(),
      content: contentString
    })
  }

  async _onWealthCalc(event) {
    event.preventDefault()

    let d = new Dialog({
      title: "Argent",
      content: `<form>
                <div class="dialogForm">
                  <div style="display: flex; flex-direction: row; justify-content: space-between; align-items: center;">
                    <label><i class="fas fa-coins"></i><b> Ajouter/Soustraire: </b></label>
                    <input placeholder="ex. -20, +10" id="playerInput" value="0" style=" text-align: center; width: 50%; border-style: groove; float: right;" type="text"></input></div>
                  </div>
                </form>`,
      buttons: {
        one: {
          label: "Annuler",
          callback: html => console.log("Cancelled")
        },
        two: {
          label: "Valider",
          callback: async (html) => {
            const playerInput = parseInt(html.find('[id="playerInput"]').val());
            let wealth = this.actor.system.wealth;

            wealth = wealth + playerInput;
            this.actor.update({"data.wealth" : wealth});

          }
        }
      },
      default: "two",
      close: html => console.log()
    })
    d.render(true);
  }

  async _onCarryBonus(event) {
    event.preventDefault()

    let d = new Dialog({
      title: "Carry Rating Bonus",
      content: `<form>
                  <div class="dialogForm">
                    <div style="margin: 5px; display: flex; flex-direction: row; justify-content: space-between; align-items: center;">
                      <label><b>Current Carry Rating Bonus: </b></label>
                      <label style=" text-align: center; float: right; width: 50%;">${this.actor.system.carry_rating.bonus}</label>
                    </div>

                    <div style="margin: 5px; display: flex; flex-direction: row; justify-content: space-between; align-items: center;">
                      <label><b> Set Carry Weight Bonus:</b></label>
                      <input placeholder="10, -10, etc." id="playerInput" value="0" style=" text-align: center; width: 50%; border-style: groove; float: right;" type="text"></input></div>
                    </div>

                </form>`,
      buttons: {
        one: {
          label: "Cancel",
          callback: html => console.log("Cancelled")
        },
        two: {
          label: "Submit",
          callback: async (html) => {
            const playerInput = parseInt(html.find('[id="playerInput"]').val());
            this.actor.system.carry_rating.bonus = playerInput;
            this.actor.update({"data.carry_rating.bonus" : this.actor.system.carry_rating.bonus});
          }
        }
      },
      default: "two",
      close: html => console.log()
    })
    d.render(true);
  }

  _onIncrementResource(event) {
    event.preventDefault()
    const resource = this.actor.system[event.currentTarget.dataset.resource]
    const action = event.currentTarget.dataset.action
    let dataPath = `data.${event.currentTarget.dataset.resource}.value`
    
    // Update and increment resource
    action == 'increase' ? this.actor.update({[dataPath]: resource.value + 1}) : this.actor.update({[dataPath]: resource.value - 1})
  }

  _onResetResource(event) {
    event.preventDefault()
    const resourceLabel = event.currentTarget.dataset.resource
    const resource = this.actor.system[resourceLabel]
    let dataPath = `data.${resourceLabel}.value`

    this.actor.update({[dataPath]: resource.value = resource.max})
  }



  _setResourceBars() {
    const data = this.actor.system

    if (data) {
        for (let bar of [...this.form.querySelectorAll('.currentBar')]) {
          let resource = data[bar.dataset.resource]

          if (resource.max !== 0) {
              let resourceElement = this.form.querySelector(`#${bar.id}`)
              let proportion = Number((100 * (resource.value / resource.max)).toFixed(0))

              // if greater than 100 or lower than 20, set values to fit bars correctly
              proportion < 100 ? proportion = proportion : proportion = 100
              proportion < 0 ? proportion = 0 : proportion = proportion

              // Apply the proportion to the width of the resource bar
              resourceElement.style.width = `${proportion}%`
          }
        }
      }
  }

  _createSpellFilterOptions() {
    for (let spell of this.actor.items.filter(item => item.type === 'spell')) {
      if ([...this.form.querySelectorAll('#spellFilter option')].some(i => i.innerHTML === spell.system.school)) {continue}
      else {
        let option = document.createElement('option')
        option.innerHTML = spell.system.school
        this.form.querySelector('#spellFilter').append(option)
      }
    }
  }

  _createItemFilterOptions() {
    for (let item of this.actor.items.filter(i => i.system.hasOwnProperty('equipped') && i.system.equipped === false)) {
      if ([...this.form.querySelectorAll('#itemFilter option')].some(i => i.innerHTML === item.type)) {continue}
      else {
        let option = document.createElement('option')
        option.innerHTML = item.type === 'ammunition' ? 'ammo' : item.type
        option.value = item.type
        this.form.querySelector('#itemFilter').append(option)
      }
    }
  }

  _filterSpells(event) {
    event.preventDefault()
    let filterBy = event.currentTarget.value
    
    for (let spellItem of [...this.form.querySelectorAll(".spellList tbody .item")]) {
      switch (filterBy) {
        case 'All':
          spellItem.classList.add('active')
          sessionStorage.setItem('savedSpellFilter', filterBy)
          break
          
        case `${filterBy}`:
          filterBy == spellItem.dataset.spellSchool ? spellItem.classList.add('active') : spellItem.classList.remove('active')
          sessionStorage.setItem('savedSpellFilter', filterBy)
          break
      }
    }
  }

  _filterItems(event) {
    event.preventDefault()
    let filterBy = event.currentTarget.value
    
    for (let item of [...this.form.querySelectorAll(".equipmentList tbody .item")]) {
      switch (filterBy) {
        case 'All':
          item.classList.add('active')
          sessionStorage.setItem('savedItemFilter', filterBy)
          break
          
        case `${filterBy}`:
          filterBy == item.dataset.itemType ? item.classList.add('active') : item.classList.remove('active')
          sessionStorage.setItem('savedItemFilter', filterBy)
          break
      }
    }
  }

  _setDefaultItemFilter() {
    let filterBy = sessionStorage.getItem('savedItemFilter')

    if (filterBy !== null||filterBy !== undefined) {
      this.form.querySelector('#itemFilter').value = filterBy
      for (let item of [...this.form.querySelectorAll('.equipmentList tbody .item')]) {
        switch (filterBy) {
          case 'All':
            item.classList.add('active')
            break

          case `${filterBy}`:
            filterBy == item.dataset.itemType ? item.classList.add('active') : item.classList.remove('active')
            break
        }
      }
    }
}

  _setDefaultSpellFilter() {
      let filterBy = sessionStorage.getItem('savedSpellFilter')

      if (filterBy !== null||filterBy !== undefined) {
        this.form.querySelector('#spellFilter').value = filterBy
        for (let spellItem of [...this.form.querySelectorAll('.spellList tbody .item')]) {
          switch (filterBy) {
            case 'All':
              spellItem.classList.add('active')
              break

            case `${filterBy}`:
              filterBy == spellItem.dataset.spellSchool ? spellItem.classList.add('active') : spellItem.classList.remove('active')
              break
          }
        }
      }
  }

  _createStatusTags() {
    this.actor.system.wounded ? this.form.querySelector('#wound-icon').classList.add('active') : this.form.querySelector('#wound-icon').classList.remove('active')
    this.actor.system.carry_rating.current > this.actor.system.carry_rating.max ? this.form.querySelector('#enc-icon').classList.add('active') : this.form.querySelector('#enc-icon').classList.remove('active')
    }

  _selectCombatRank(event) {
    event.preventDefault()
    let element = event.currentTarget

    let combatStyle = this.actor.getEmbeddedDocument('Item', element.id)
    combatStyle.update({'data.rank': element.value})
    element.querySelector(`[value="${element.value}"]`).selected = true

  }

  _setDefaultCombatRank() {
    for (let rankElement of [...this.form.querySelectorAll('.rank-select')]) {
      let item = this.actor.getEmbeddedDocument('Item', rankElement.id)
      let option = rankElement.querySelector(`[value="${item.system.rank}"]`)
      option.selected = true
    }
  }

  _onEquipItems(event) {
    event.preventDefault()
    let element = event.currentTarget
    let itemList = this.actor.items.filter(item => item.type === element.id||(item.type === element.dataset.altType && item.system.wearable))

    let itemEntries = []
    let tableHeader = ''
    let tableEntry = ''

    // Loop through Item List and create table rows
    for (let item of itemList) {
      switch (item.type) {
        case 'armor':
        case 'item':
          tableEntry = `<tr>
                            <td data-item-id="${item._id}">
                                <div style="display: flex; flex-direction: row; align-items: center; gap: 5px;">
                                  <img class="item-img" src="${item.img}" height="24" width="24">
                                  ${item.name}
                                </div>
                            </td>
                            <td style="text-align: center;">${item.system.armor}</td>
                            <td style="text-align: center;">${item.system.magic_ar}</td>
                            <td style="text-align: center;">
                                <input type="checkbox" class="itemSelect" data-item-id="${item._id}" ${item.system.equipped ? 'checked' : ''}>
                            </td>
                        </tr>`
                        break

        case 'weapon':
          tableEntry = `<tr>
                            <td data-item-id="${item._id}">
                                <div style="display: flex; flex-direction: row; align-items: center; gap: 5px;">
                                  <img class="item-img" src="${item.img}" height="24" width="24">
                                  ${item.name}
                                </div>
                            </td>
                            <td style="text-align: center;">${item.system.damage}</td>
                            <td style="text-align: center;">${item.data.data.de3}</td>
                            <td style="text-align: center;">${item.data.data.tour}</td>
                            <td style="text-align: center;">${item.system.reach}</td>
                            <td style="text-align: center;">
                                <input type="checkbox" class="itemSelect" data-item-id="${item._id}" ${item.system.equipped ? 'checked' : ''}>
                            </td>
                        </tr>`
                        break

        case 'accessoire':
          tableEntry = `<tr>
                            <td data-item-id="${item._id}">
                                <div style="display: flex; flex-direction: row; align-items: center; gap: 5px;">
                                  <img class="item-img" src="${item.img}" height="24" width="24">
                                  ${item.name}
                                </div>
                            </td>
                            <td style="text-align: center;">${item.system.damage}</td>
                            <td style="text-align: center;">${item.data.data.de3}</td>
                            <td style="text-align: center;">${item.data.data.tour}</td>
                            <td style="text-align: center;">${item.system.reach}</td>
                            <td style="text-align: center;">
                                <input type="checkbox" class="itemSelect" data-item-id="${item._id}" ${item.system.equipped ? 'checked' : ''}>
                            </td>
                        </tr>`
                        break

        case 'ammunition':
          tableEntry = `<tr>
                            <td data-item-id="${item._id}">
                                <div style="display: flex; flex-direction: row; align-items: center; gap: 5px;">
                                  <img class="item-img" src="${item.img}" height="24" width="24">
                                  ${item.name}
                                </div>
                            </td>
                            <td style="text-align: center;">${item.system.quantity}</td>
                            <td style="text-align: center;">${item.system.damage}</td>
                            <td style="text-align: center;">${item.system.enchant_level}</td>
                            <td style="text-align: center;">
                                <input type="checkbox" class="itemSelect" data-item-id="${item._id}" ${item.system.equipped ? 'checked' : ''}>
                            </td>
                        </tr>`
                        break
      }

      itemEntries.push(tableEntry)
    }

    // Find first entry and determine item type to create appropriate item header
    if (itemList.length === 0) {return ui.notifications.info(`${this.actor.name} does not have any items of this type to equip.`)}
    switch (itemList[0].type) {
      case 'armor':
      case 'item':
        tableHeader = `<div>
                          <div style="padding: 5px 0;">
                              <label>Ne rien sélectionner déséquipera tous les objets</label>
                          </div>

                          <div>
                              <table>
                                  <thead>
                                      <tr>
                                          <th>Nom</th>
                                          <th>PHY</th>
                                          <th>MAG</th>
                                          <th>PDS</th>
                                          <th>Equipée</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      ${itemEntries.join('')}
                                  </tbody>
                              </table>
                          </div>
                      </div>`
                      break

      case 'weapon': 
        tableHeader = `<div>
                          <div style="padding: 5px 0;">
                              <label>Ne rien sélectionner déséquipera tous les objets</label>
                          </div>

                          <div>
                              <table>
                                  <thead>
                                      <tr>
                                          <th>Nom</th>
                                          <th>Dégâts</th>
                                          <th>Dé+</th>
                                          <th>Tour</th>
                                          <th>Portée</th>
                                          <th>Equipée</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      ${itemEntries.join('')}
                                  </tbody>
                              </table>
                          </div>
                      </div>`
                      break

      case 'accessoire': 
        tableHeader = `<div>
                          <div style="padding: 5px 0;">
                              <label>Ne rien sélectionner déséquipera tous les objets</label>
                          </div>

                          <div>
                              <table>
                                  <thead>
                                      <tr>
                                          <th>Nom</th>
                                          <th>Dégâts</th>
                                          <th>Dé+</th>
                                          <th>Tour</th>
                                          <th>Portée</th>
                                          <th>Equipée</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                      ${itemEntries.join('')}
                                  </tbody>
                              </table>
                          </div>
                      </div>`
                      break

      case 'ammunition': 
      tableHeader = `<div>
                        <div style="padding: 5px 0;">
                            <label>Selecting nothing will unequip all items</label>
                        </div>

                        <div>
                            <table>
                                <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th>Qté</th>
                                        <th>Dégats</th>
                                        <th>Enchant</th>
                                        <th>Equipée</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    ${itemEntries.join('')}
                                </tbody>
                            </table>
                        </div>
                    </div>`
    }

    let d = new Dialog({
      title: "Item List",
      content: tableHeader,
      buttons: {
        one: {
          label: "Annuler",
          callback: html => console.log('Cancelled')
        },
        two: {
          label: "Valider",
          callback: async html => {
                let selectedArmor = [...document.querySelectorAll('.itemSelect')]

                for (let armorItem of selectedArmor) {
                  let thisArmor = this.actor.items.filter(item => item.id == armorItem.dataset.itemId)[0]
                  armorItem.checked ? await thisArmor.update({'data.equipped': true}) : await thisArmor.update({'data.equipped': false})
                }
          }
        }
      },
      default: "two",
      close: html => console.log()
    })
    
    d.position.width = 500
    d.render(true)
  }
}

