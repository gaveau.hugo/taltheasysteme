/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */

export class SimpleActor extends Actor {
  async _preCreate(data, options, user) {

    if (this.type === 'character') {
      // Updates token default settings for Character types
      this.prototypeToken.updateSource({
        'sight.enabled': true, 
        actorLink: true, 
        disposition: 1
      })
    }

    // Preps and adds standard skill items to Character types
    await super._preCreate(data, options, user);
    if (this.type === "character") {
      let skillPack = game.packs.get("talthea-system.standard-skills");
      let collection = await skillPack.getDocuments();
      collection.sort(function (a, b) {
        let nameA = a.name.toUpperCase();
        let nameB = b.name.toUpperCase();
        if (nameA < nameB) {
          return -1;
        } if (nameA > nameB) {
          return 1;
        }
        return 0
      });

      this.updateSource({
        _id: this._id,
        items: collection.map(i => i.toObject()),
        'system.size': 'standard'
      })
    }
  }

  prepareData() {
    super.prepareData();

    const actorData = this;
    const data = actorData.system;
    const flags = actorData.flags;

    // Make separate methods for each Actor type (character, npc, etc.) to keep
    // things organized.
    if (actorData.type === 'character') this._prepareCharacterData(actorData);
    if (actorData.type === 'npc') this._prepareNPCData(actorData);
  }

  /**
   * Prepare Character type specific data
   */
  _prepareCharacterData(actorData) {
    const data = actorData.system;

    //Add bonuses from items to Characteristics
    data.characteristics.for.total = data.characteristics.for.base + this._forBonusCalc(actorData);
    data.characteristics.end.total = data.characteristics.end.base + this._endBonusCalc(actorData);
    data.characteristics.agi.total = data.characteristics.agi.base + this._agiBonusCalc(actorData) - this._armorWeight(actorData);
    data.characteristics.int.total = data.characteristics.int.base + this._intBonusCalc(actorData);
    data.characteristics.wp.total = data.characteristics.wp.base + this._wpBonusCalc(actorData);
    data.characteristics.prc.total = data.characteristics.prc.base + this._prcBonusCalc(actorData);
    data.characteristics.prs.total = data.characteristics.prs.base + this._prsBonusCalc(actorData);
    data.characteristics.lck.total = data.characteristics.lck.base + this._lckBonusCalc(actorData);
    data.characteristics.sag.total = data.characteristics.sag.base + this._sagBonusCalc(actorData);
    data.characteristics.elo.total = data.characteristics.elo.base + this._eloBonusCalc(actorData);

    //Add bonuses from items to Compétences
    data.characteristics.for.competences.parade.total = data.characteristics.for.competences.parade.base + this._paradeBonusCalc(actorData);
    data.characteristics.for.competences.lancer.total = data.characteristics.for.competences.lancer.base + this._lancerBonusCalc(actorData);
    data.characteristics.for.competences.mains_nues.total = data.characteristics.for.competences.mains_nues.base + this._mains_nuesBonusCalc(actorData);
    data.characteristics.for.competences.armes_moyennes_lourdes.total = data.characteristics.for.competences.armes_moyennes_lourdes.base + this._armes_moyennes_lourdesBonusCalc(actorData);
    data.characteristics.for.competences.endurance.total = data.characteristics.for.competences.endurance.base + this._enduranceBonusCalc(actorData);
    data.characteristics.for.competences.intimidation.total = data.characteristics.for.competences.intimidation.base + this._intimidationBonusCalc(actorData);
    data.characteristics.agi.competences.esquive.total = data.characteristics.agi.competences.esquive.base + this._esquiveBonusCalc(actorData);
    data.characteristics.agi.competences.discretion.total = data.characteristics.agi.competences.discretion.base + this._discretionBonusCalc(actorData);
    data.characteristics.agi.competences.crochetage.total = data.characteristics.agi.competences.crochetage.base + this._crochetageBonusCalc(actorData);
    data.characteristics.agi.competences.initiative.total = data.characteristics.agi.competences.initiative.base + this._initiativeBonusCalc(actorData);
    data.characteristics.agi.competences.acrobatie.total = data.characteristics.agi.competences.acrobatie.base + this._acrobatieBonusCalc(actorData);
    data.characteristics.agi.competences.armes_courtes_distances.total = data.characteristics.agi.competences.armes_courtes_distances.base + this._armes_courtes_distancesBonusCalc(actorData);
    data.characteristics.int.competences.histoire.total = data.characteristics.int.competences.histoire.base + this._histoireBonusCalc(actorData);
    data.characteristics.int.competences.langues.total = data.characteristics.int.competences.langues.base + this._languesBonusCalc(actorData);
    data.characteristics.int.competences.alchimie.total = data.characteristics.int.competences.alchimie.base + this._alchimieBonusCalc(actorData);
    data.characteristics.int.competences.medecine.total = data.characteristics.int.competences.medecine.base + this._medecineBonusCalc(actorData);
    data.characteristics.int.competences.arcanes.total = data.characteristics.int.competences.arcanes.base + this._arcanesBonusCalc(actorData);
    data.characteristics.int.competences.nature.total = data.characteristics.int.competences.nature.base + this._natureBonusCalc(actorData);
    data.characteristics.prc.competences.reflexe.total = data.characteristics.prc.competences.reflexe.base + this._reflexeBonusCalc(actorData);
    data.characteristics.prc.competences.ecoute.total = data.characteristics.prc.competences.ecoute.base + this._ecouteBonusCalc(actorData);
    data.characteristics.prc.competences.detectionM.total = data.characteristics.prc.competences.detectionM.base + this._detectionMBonusCalc(actorData);
    data.characteristics.prc.competences.reperage.total = data.characteristics.prc.competences.reperage.base + this._reperageBonusCalc(actorData);
    data.characteristics.prc.competences.discernement.total = data.characteristics.prc.competences.discernement.base + this._discernementBonusCalc(actorData);
    data.characteristics.prc.competences.pistage.total = data.characteristics.prc.competences.pistage.base + this._pistageBonusCalc(actorData);
    data.characteristics.sag.competences.concentration.total = data.characteristics.sag.competences.concentration.base + this._concentrationBonusCalc(actorData);
    data.characteristics.sag.competences.intuition.total = data.characteristics.sag.competences.intuition.base + this._intuitionBonusCalc(actorData);
    data.characteristics.sag.competences.survie.total = data.characteristics.sag.competences.survie.base + this._survieBonusCalc(actorData);
    data.characteristics.sag.competences.premiers_soins.total = data.characteristics.sag.competences.premiers_soins.base + this._premiers_soinsBonusCalc(actorData);
    data.characteristics.sag.competences.intuitionM.total = data.characteristics.sag.competences.intuitionM.base + this._intuitionMBonusCalc(actorData);
    data.characteristics.sag.competences.artisanats.total = data.characteristics.sag.competences.artisanats.base + this._artisanatsBonusCalc(actorData);
    data.characteristics.elo.competences.charme.total = data.characteristics.elo.competences.charme.base + this._charmeBonusCalc(actorData);
    data.characteristics.elo.competences.spectacle.total = data.characteristics.elo.competences.spectacle.base + this._spectacleBonusCalc(actorData);
    data.characteristics.elo.competences.bluff.total = data.characteristics.elo.competences.bluff.base + this._bluffBonusCalc(actorData);
    data.characteristics.elo.competences.mensonge.total = data.characteristics.elo.competences.mensonge.base + this._mensongeBonusCalc(actorData);
    data.characteristics.elo.competences.negociation.total = data.characteristics.elo.competences.negociation.base + this._negociationBonusCalc(actorData);
    data.characteristics.elo.competences.troc.total = data.characteristics.elo.competences.troc.base + this._trocBonusCalc(actorData);

    //Characteristic Bonuses
    var forBonus = Math.floor(data.characteristics.for.total / 10);
    var endBonus = Math.floor(data.characteristics.end.total / 10);
    var agiBonus = Math.floor(data.characteristics.agi.total / 10);
    var intBonus = Math.floor(data.characteristics.int.total / 10);
    var wpBonus = Math.floor(data.characteristics.wp.total / 10);
    var prcBonus = Math.floor(data.characteristics.prc.total / 10);
    var prsBonus = Math.floor(data.characteristics.prs.total / 10);
    var lckBonus = Math.floor(data.characteristics.lck.total / 10);
    var sagBonus = Math.floor(data.characteristics.sag.total / 10);
    var eloBonus = Math.floor(data.characteristics.elo.total / 10);

  //Set Campaign Rank
  if (data.xpTotal >= 5000) {
    data.campaignRank = "Master"
  } else if (data.xpTotal >= 4000) {
    data.campaignRank = "Expert"
  } else if (data.xpTotal >= 3000) {
    data.campaignRank = "Adept"
  } else if (data.xpTotal >= 2000) {
    data.campaignRank = "Journeyman"
  } else {
    data.campaignRank = "Apprentice"
  }

    //Talent/Power/Trait Resource Bonuses
    data.hp.bonus = this._hpBonus(actorData);
    data.magicka.bonus = this._mpBonus(actorData);
    data.stamina.bonus = this._spBonus(actorData);
    data.wound_threshold.bonus = this._wtBonus(actorData);
    data.speed.bonus = this._speedBonus(actorData);
    data.initiative.bonus = this._iniBonus(actorData);

    //Talent/Power/Trait Resistance Bonuses
    data.resistance.physicRBonus = this._physicRBonus(actorData);
    data.resistance.magicRBonus = this._magicRBonus(actorData);
    data.resistance.fireRBonus = this._fireRBonus(actorData);
    data.resistance.frostRBonus = this._frostRBonus(actorData);
    data.resistance.waterRBonus = this._waterRBonus(actorData);
    data.resistance.airRBonus = this._airRBonus(actorData);
    data.resistance.poisonRBonus = this._poisonRBonus(actorData);
    data.resistance.earthRBonus = this._earthRBonus(actorData);
    data.resistance.natureRBonus = this._natureRBonus(actorData);
    data.resistance.windRBonus = this._windRBonus(actorData);
    data.resistance.electricityRBonus = this._electricityRBonus(actorData);
    data.resistance.darkRBonus = this._darkRBonus(actorData);
    data.resistance.lightRBonus = this._lightRBonus(actorData);

    //Status Bonuses
    data.resistance.burnBonus = this._burnBonus(actorData);
    data.resistance.wetBonus = this._wetBonus(actorData);
    data.resistance.freezeBonus = this._freezeBonus(actorData);
    data.resistance.paralysisBonus = this._paralysisBonus(actorData);
    data.resistance.slowBonus = this._slowBonus(actorData);
    data.resistance.flashBonus = this._flashBonus(actorData);
    data.resistance.darkBonus = this._darkBonus(actorData);
    data.resistance.poisonBonus = this._poisonBonus(actorData);

    //Talent/Power/Trait modif dgts Bonuses
    data.modifDgts.physicDBonus = this._physicDBonus(actorData);
    data.modifDgts.magicDBonus = this._magicDBonus(actorData);
    data.modifDgts.fireDBonus = this._fireDBonus(actorData);
    data.modifDgts.frostDBonus = this._frostDBonus(actorData);
    data.modifDgts.waterDBonus = this._waterDBonus(actorData);
    data.modifDgts.airDBonus = this._airDBonus(actorData);
    data.modifDgts.poisonDBonus = this._poisonDBonus(actorData);
    data.modifDgts.earthDBonus = this._earthDBonus(actorData);
    data.modifDgts.natureDBonus = this._natureDBonus(actorData);
    data.modifDgts.windDBonus = this._windDBonus(actorData);
    data.modifDgts.electricityDBonus = this._electricityDBonus(actorData);
    data.modifDgts.darkDBonus = this._darkDBonus(actorData);
    data.modifDgts.lightDBonus = this._lightDBonus(actorData);

    //Derived Calculations
    if (this._isMechanical(actorData) == true) {
      data.wound_threshold.base = forBonus + (endBonus * 2);
    } else {
      data.wound_threshold.base = forBonus + endBonus + wpBonus + (data.wound_threshold.bonus);
    }
    data.wound_threshold.value = data.wound_threshold.base;
    data.wound_threshold.value = this._woundThresholdCalc(actorData);
    
    data.speed.base = forBonus + (2 * agiBonus) + (data.speed.bonus);
    data.speed.value = this._speedCalc(actorData);
    data.speed.swimSpeed = parseFloat(this._swimCalc(actorData)) + parseFloat((data.speed.value/2).toFixed(0));
    data.speed.flySpeed = this._flyCalc(actorData);

    data.initiative.base = agiBonus + intBonus + prcBonus + (data.initiative.bonus);
    data.initiative.value = data.initiative.base;
    data.initiative.value = this._iniCalc(actorData);

    data.hp.base = Math.ceil(data.characteristics.end.total / 2);
    data.hp.max;

    data.magicka.max;

    data.stamina.max = endBonus + data.stamina.bonus;

    data.carry_rating.max = Math.floor((4 * forBonus) + (2 * endBonus)) + data.carry_rating.bonus;
    data.carry_rating.current = (this._calculateENC(actorData) - this._armorWeight(actorData) - this._excludeENC(actorData)).toFixed(1);


    data.resistance.physicR.max = data.resistance.physicR.base + this._physicRBonus(actorData);
    data.resistance.magicR.max = data.resistance.magicR.base + this._magicRBonus(actorData);
    data.resistance.fireR.max = data.resistance.fireR.base + this._fireRBonus(actorData);
    data.resistance.frostR.max = data.resistance.frostR.base + this._frostRBonus(actorData);
    data.resistance.waterR.max = data.resistance.waterR.base + this._waterRBonus(actorData);
    data.resistance.airR.max = data.resistance.airR.base + this._airRBonus(actorData);
    data.resistance.poisonR.max = data.resistance.poisonR.base + this._poisonRBonus(actorData);
    data.resistance.earthR.max = data.resistance.earthR.base + this._earthRBonus(actorData);
    data.resistance.natureR.max = data.resistance.natureR.base + this._natureRBonus(actorData);
    data.resistance.windR.max = data.resistance.windR.base + this._windRBonus(actorData);
    data.resistance.electricityR.max = data.resistance.electricityR.base + this._electricityRBonus(actorData);
    data.resistance.darkR.max = data.resistance.darkR.base + this._darkRBonus(actorData);
    data.resistance.lightR.max = data.resistance.lightR.base + this._lightRBonus(actorData);


    //Talent/Power/Trait modif dgts Bonuses
    data.modifDgts.physicD.max = data.modifDgts.physicD.base + this._physicDBonus(actorData);
    data.modifDgts.magicD.max = data.modifDgts.magicD.base + this._magicDBonus(actorData);
    data.modifDgts.fireD.max = data.modifDgts.fireD.base + this._fireDBonus(actorData);
    data.modifDgts.frostD.max = data.modifDgts.frostD.base + this._frostDBonus(actorData);
    data.modifDgts.waterD.max = data.modifDgts.waterD.base + this._waterDBonus(actorData);
    data.modifDgts.airD.max = data.modifDgts.airD.base + this._airDBonus(actorData);
    data.modifDgts.poisonD.max = data.modifDgts.poisonD.base + this._poisonDBonus(actorData);
    data.modifDgts.earthD.max = data.modifDgts.earthD.base + this._earthDBonus(actorData);
    data.modifDgts.natureD.max = data.modifDgts.natureD.base + this._natureDBonus(actorData);
    data.modifDgts.windD.max = data.modifDgts.windD.base + this._windDBonus(actorData);
    data.modifDgts.electricityD.max = data.modifDgts.electricityD.base + this._electricityDBonus(actorData);
    data.modifDgts.darkD.max = data.modifDgts.darkD.base + this._darkDBonus(actorData);
    data.modifDgts.lightD.max = data.modifDgts.lightD.base + this._lightDBonus(actorData);

    //Status Bonuses
    data.effect_cat.burn.max = data.effect_cat.burn.base + this._burnBonus(actorData);
    data.effect_cat.freeze.max = data.effect_cat.freeze.base + this._freezeBonus(actorData);
    data.effect_cat.paralysis.max = data.effect_cat.paralysis.base + this._paralysisBonus(actorData);
    data.effect_cat.slow.max = data.effect_cat.slow.base + this._slowBonus(actorData);
    data.effect_cat.flash.max = data.effect_cat.flash.base + this._flashBonus(actorData);
    data.effect_cat.dark.max = data.effect_cat.dark.base + this._darkBonus(actorData);
    data.effect_cat.poison.max = data.effect_cat.poison.base + this._poisonBonus(actorData);


    //Form Shift Calcs
    if (this._wereWolfForm(actorData) === true) {
      data.resistance.silverR = data.resistance.silverR - 5;
      data.resistance.diseaseR = data.resistance.diseaseR + 200;
      data.hp.max = data.hp.max + 5;
      data.stamina.max = data.stamina.max + 1;
      data.speed.base = data.speed.base + 9;
      data.speed.value = this._speedCalc(actorData);
      data.speed.swimSpeed = (data.speed.value/2).toFixed(0);
      data.resistance.natToughness = 5;
      data.wound_threshold.value = data.wound_threshold.value + 5;
      data.action_points.max = data.action_points.max - 1;
      actorData.items.find(i => i.name === 'Survival').data.data.miscValue = 30;
      actorData.items.find(i => i.name === 'Navigate').data.data.miscValue = 30;
      actorData.items.find(i => i.name === 'Observe').data.data.miscValue = 30;
    } else if (this._wereBatForm(actorData) === true) {
        data.resistance.silverR = data.resistance.silverR - 5;
        data.resistance.diseaseR = data.resistance.diseaseR + 200;
        data.hp.max = data.hp.max + 5;
        data.stamina.max = data.stamina.max + 1;
        data.speed.value = (this._speedCalc(actorData)/2).toFixed(0);
        data.speed.flySpeed = data.speed.base + 9;
        data.speed.swimSpeed = (data.speed.value/2).toFixed(0);
        data.resistance.natToughness = 5;
        data.wound_threshold.value = data.wound_threshold.value + 3;
        data.action_points.max = data.action_points.max - 1;
        actorData.items.find(i => i.name === 'Survival').data.data.miscValue = 30;
      actorData.items.find(i => i.name === 'Navigate').data.data.miscValue = 30;
      actorData.items.find(i => i.name === 'Observe').data.data.miscValue = 30;
    } else if (this._wereBoarForm(actorData) === true) {
        data.resistance.silverR = data.resistance.silverR - 5;
        data.resistance.diseaseR = data.resistance.diseaseR + 200;
        data.hp.max = data.hp.max + 5;
        data.speed.base = data.speed.base + 9;
        data.speed.value = this._speedCalc(actorData);
        data.speed.swimSpeed = (data.speed.value/2).toFixed(0);
        data.resistance.natToughness = 7;
        data.wound_threshold.value = data.wound_threshold.value + 5;
        data.action_points.max = data.action_points.max - 1;
        actorData.items.find(i => i.name === 'Survival').data.data.miscValue = 30;
        actorData.items.find(i => i.name === 'Navigate').data.data.miscValue = 30;
        actorData.items.find(i => i.name === 'Observe').data.data.miscValue = 30;
    } else if (this._wereBearForm(actorData) === true) {
        data.resistance.silverR = data.resistance.silverR - 5;
        data.resistance.diseaseR = data.resistance.diseaseR + 200;
        data.hp.max = data.hp.max + 10;
        data.stamina.max = data.stamina.max + 1;
        data.speed.base = data.speed.base + 5;
        data.speed.value = this._speedCalc(actorData);
        data.speed.swimSpeed = (data.speed.value/2).toFixed(0);
        data.resistance.natToughness = 5;
        data.wound_threshold.value = data.wound_threshold.value + 5;
        data.action_points.max = data.action_points.max - 1;
        actorData.items.find(i => i.name === 'Survival').data.data.miscValue = 30;
        actorData.items.find(i => i.name === 'Navigate').data.data.miscValue = 30;
        actorData.items.find(i => i.name === 'Observe').data.data.miscValue = 30;
    } else if (this._wereCrocodileForm(actorData) === true) {
        data.resistance.silverR = data.resistance.silverR - 5;
        data.resistance.diseaseR = data.resistance.diseaseR + 200;
        data.hp.max = data.hp.max + 5;
        data.stamina.max = data.stamina.max + 1;
        data.speed.value = (this._addHalfSpeed(actorData)).toFixed(0);
        data.speed.swimSpeed = parseFloat(this._speedCalc(actorData)) + 9;
        data.resistance.natToughness = 5;
        data.wound_threshold.value = data.wound_threshold.value + 5;
        data.action_points.max = data.action_points.max - 1;
        actorData.items.find(i => i.name === 'Survival').data.data.miscValue = 30;
        actorData.items.find(i => i.name === 'Navigate').data.data.miscValue = 30;
        actorData.items.find(i => i.name === 'Observe').data.data.miscValue = 30;
    } else if (this._wereVultureForm(actorData) === true) {
        data.resistance.silverR = data.resistance.silverR - 5;
        data.resistance.diseaseR = data.resistance.diseaseR + 200;
        data.hp.max = data.hp.max + 5;
        data.stamina.max = data.stamina.max + 1;
        data.speed.value = (this._speedCalc(actorData)/2).toFixed(0);
        data.speed.flySpeed = data.speed.base + 9;
        data.speed.swimSpeed = (data.speed.value/2).toFixed(0);
        data.resistance.natToughness = 5;
        data.wound_threshold.value = data.wound_threshold.value + 3;
        data.action_points.max = data.action_points.max - 1;
        actorData.items.find(i => i.name === 'Survival').data.data.miscValue = 30;
        actorData.items.find(i => i.name === 'Navigate').data.data.miscValue = 30;
        actorData.items.find(i => i.name === 'Observe').data.data.miscValue = 30;
    } else if (this._vampireLordForm(actorData) === true) {
        data.resistance.fireR = data.resistance.fireR - 1;
        data.resistance.sunlightR = data.resistance.sunlightR - 1;
        data.speed.flySpeed = 5;
        data.hp.max = data.hp.max + 5;
        data.magicka.max = data.magicka.max + 25;
        data.resistance.natToughness = 3;
    }

    //Speed Recalculation
    data.speed.value = this._addHalfSpeed(actorData);

    //ENC Burden Calculations
    if (data.carry_rating.current > data.carry_rating.max * 3) {
      data.carry_rating.label = 'Crushing'
      data.carry_rating.penalty = -40
      data.speed.value = 0;
      data.stamina.max = data.stamina.max - 5;
    } else if (data.carry_rating.current > data.carry_rating.max * 2) {
      data.carry_rating.label = 'Severe'
      data.carry_rating.penalty = -20
      data.speed.value = Math.floor(data.speed.base / 2);
      data.stamina.max = data.stamina.max - 3;
    } else if (data.carry_rating.current > data.carry_rating.max) {
      data.carry_rating.label = 'Moderate'
      data.carry_rating.penalty = -10
      data.speed.value = data.speed.value - 1;
      data.stamina.max = data.stamina.max - 1;
    } else if (data.carry_rating.current <= data.carry_rating.max) {
      data.carry_rating.label = "Minimal"
      data.carry_rating.penalty = 0
    }

    //Armor Weight Class Calculations
    if (data.armor_class == "super_heavy") {
      data.speed.value = data.speed.value - 3;
      data.speed.swimSpeed = data.speed.swimSpeed - 3;
    } else if (data.armor_class == "heavy") {
      data.speed.value = data.speed.value - 2;
      data.speed.swimSpeed = data.speed.swimSpeed - 2;
    } else if (data.armor_class == "medium") {
      data.speed.value = data.speed.value - 1;
      data.speed.swimSpeed = data.speed.swimSpeed - 1;
    } else {
      data.speed.value = data.speed.value;
      data.speed.swimSpeed = data.speed.swimSpeed;
    }

    //Wounded Penalties
    if (data.wounded == true) {
      let woundPen = 0
      let woundIni = -2;
      this._painIntolerant(actorData) ? woundPen = -30 : woundPen = -20

      if (this._halfWoundPenalty(actorData) === true) {
        data.woundPenalty = woundPen / 2
        data.initiative.value = data.initiative.base + (woundIni / 2);

      } else if (this._halfWoundPenalty(actorData) === false) {
        data.initiative.value = data.initiative.base + woundIni;
        data.woundPenalty = woundPen;
      }
    }

  } 

  async _prepareNPCData(actorData) {
    const data = actorData.system;

    //Add bonuses from items to Characteristics
    data.characteristics.for.total = data.characteristics.for.base + this._forBonusCalc(actorData);
    data.characteristics.end.total = data.characteristics.end.base + this._endBonusCalc(actorData);
    data.characteristics.agi.total = data.characteristics.agi.base + this._agiBonusCalc(actorData);
    data.characteristics.int.total = data.characteristics.int.base + this._intBonusCalc(actorData);
    data.characteristics.wp.total = data.characteristics.wp.base + this._wpBonusCalc(actorData);
    data.characteristics.prc.total = data.characteristics.prc.base + this._prcBonusCalc(actorData);
    data.characteristics.prs.total = data.characteristics.prs.base + this._prsBonusCalc(actorData);
    data.characteristics.lck.total = data.characteristics.lck.base + this._lckBonusCalc(actorData);
    data.characteristics.sag.total = data.characteristics.sag.base + this._sagBonusCalc(actorData);
    data.characteristics.elo.total = data.characteristics.elo.base + this._eloBonusCalc(actorData);


    //Characteristic Bonuses
    var forBonus = Math.floor(data.characteristics.for.total / 10);
    var endBonus = Math.floor(data.characteristics.end.total / 10);
    var agiBonus = Math.floor(data.characteristics.agi.total / 10);
    var intBonus = Math.floor(data.characteristics.int.total / 10);
    var wpBonus = Math.floor(data.characteristics.wp.total / 10);
    var prcBonus = Math.floor(data.characteristics.prc.total / 10);
    var prsBonus = Math.floor(data.characteristics.prs.total / 10);
    var lckBonus = Math.floor(data.characteristics.lck.total / 10);
    var sagBonus = Math.floor(data.characteristics.sag.total / 10);
    var eloBonus = Math.floor(data.characteristics.elo.total / 10);

    //Talent/Power/Trait Bonuses
    data.hp.bonus = this._hpBonus(actorData);
    data.magicka.bonus = this._mpBonus(actorData);
    data.stamina.bonus = this._spBonus(actorData);
    data.wound_threshold.bonus = this._wtBonus(actorData);
    data.speed.bonus = this._speedBonus(actorData);
    data.initiative.bonus = this._iniBonus(actorData);

    //Talent/Power/Trait Resistance Bonuses
    data.resistance.physicRBonus = this._physicRBonus(actorData);
    data.resistance.magicRBonus = this._magicRBonus(actorData);
    data.resistance.fireRBonus = this._fireRBonus(actorData);
    data.resistance.frostRBonus = this._frostRBonus(actorData);
    data.resistance.waterRBonus = this._waterRBonus(actorData);
    data.resistance.airRBonus = this._airRBonus(actorData);
    data.resistance.poisonRBonus = this._poisonRBonus(actorData);
    data.resistance.earthRBonus = this._earthRBonus(actorData);
    data.resistance.natureRBonus = this._natureRBonus(actorData);
    data.resistance.windRBonus = this._windRBonus(actorData);
    data.resistance.electricityRBonus = this._electricityRBonus(actorData);
    data.resistance.darkRBonus = this._darkRBonus(actorData);
    data.resistance.lightRBonus = this._lightRBonus(actorData);

    //Status Bonuses
    data.resistance.burnBonus = this._burnBonus(actorData);
    data.resistance.wetBonus = this._wetBonus(actorData);
    data.resistance.freezeBonus = this._freezeBonus(actorData);
    data.resistance.paralysisBonus = this._paralysisBonus(actorData);
    data.resistance.slowBonus = this._slowBonus(actorData);
    data.resistance.flashBonus = this._flashBonus(actorData);
    data.resistance.darkBonus = this._darkBonus(actorData);
    data.resistance.poisonBonus = this._poisonBonus(actorData);

    //Talent/Power/Trait modif dgts Bonuses
    data.modifDgts.physicDBonus = this._physicDBonus(actorData);
    data.modifDgts.magicDBonus = this._magicDBonus(actorData);
    data.modifDgts.fireDBonus = this._fireDBonus(actorData);
    data.modifDgts.frostDBonus = this._frostDBonus(actorData);
    data.modifDgts.waterDBonus = this._waterDBonus(actorData);
    data.modifDgts.airDBonus = this._airDBonus(actorData);
    data.modifDgts.poisonDBonus = this._poisonDBonus(actorData);
    data.modifDgts.earthDBonus = this._earthDBonus(actorData);
    data.modifDgts.natureDBonus = this._natureDBonus(actorData);
    data.modifDgts.windDBonus = this._windDBonus(actorData);
    data.modifDgts.electricityDBonus = this._electricityDBonus(actorData);
    data.modifDgts.darkDBonus = this._darkDBonus(actorData);
    data.modifDgts.lightDBonus = this._lightDBonus(actorData);

    //Derived Calculations
    if (this._isMechanical(actorData) == true) {
      data.wound_threshold.base = forBonus + (endBonus * 2);
    } else {
      data.wound_threshold.base = forBonus + endBonus + wpBonus + (data.wound_threshold.bonus);
    }
    data.wound_threshold.value = data.wound_threshold.base;
    data.wound_threshold.value = this._woundThresholdCalc(actorData);

    if (this._dwemerSphere(actorData) == true) {
      data.speed.base = 16;
      data.professions.evade = 70;
    } else {
        data.speed.base = forBonus + (2 * agiBonus) + (data.speed.bonus);
    }
    data.speed.value = this._speedCalc(actorData);
    data.speed.swimSpeed = parseFloat(this._swimCalc(actorData)) + parseFloat((data.speed.value/2).toFixed(0));
    data.speed.flySpeed = this._flyCalc(actorData);

    data.initiative.base = agiBonus + intBonus + prcBonus + (data.initiative.bonus);
    data.initiative.value = data.initiative.base;
    data.initiative.value = this._iniCalc(actorData);

    data.hp.base = Math.ceil(data.characteristics.end.total / 2);
    data.hp.max;

    data.magicka.max;

    data.stamina.max = endBonus + data.stamina.bonus;

    data.carry_rating.max = Math.floor((4 * forBonus) + (2 * endBonus)) + data.carry_rating.bonus;
    data.carry_rating.current = (this._calculateENC(actorData) - this._armorWeight(actorData) - this._excludeENC(actorData)).toFixed(1)



    data.resistance.physicR.max = data.resistance.physicR.base + this._physicRBonus(actorData);
    data.resistance.magicR.max = data.resistance.magicR.base + this._magicRBonus(actorData);
    data.resistance.fireR.max = data.resistance.fireR.base + this._fireRBonus(actorData);
    data.resistance.frostR.max = data.resistance.frostR.base + this._frostRBonus(actorData);
    data.resistance.waterR.max = data.resistance.waterR.base + this._waterRBonus(actorData);
    data.resistance.airR.max = data.resistance.airR.base + this._airRBonus(actorData);
    data.resistance.poisonR.max = data.resistance.poisonR.base + this._poisonRBonus(actorData);
    data.resistance.earthR.max = data.resistance.earthR.base + this._earthRBonus(actorData);
    data.resistance.natureR.max = data.resistance.natureR.base + this._natureRBonus(actorData);
    data.resistance.windR.max = data.resistance.windR.base + this._windRBonus(actorData);
    data.resistance.electricityR.max = data.resistance.electricityR.base + this._electricityRBonus(actorData);
    data.resistance.darkR.max = data.resistance.darkR.base + this._darkRBonus(actorData);
    data.resistance.lightR.max = data.resistance.lightR.base + this._lightRBonus(actorData);

    //Talent/Power/Trait modif dgts Bonuses
    data.modifDgts.physicD.max = data.modifDgts.physicD.base + this._physicDBonus(actorData);
    data.modifDgts.magicD.max = data.modifDgts.magicD.base + this._magicDBonus(actorData);
    data.modifDgts.fireD.max = data.modifDgts.fireD.base + this._fireDBonus(actorData);
    data.modifDgts.frostD.max = data.modifDgts.frostD.base + this._frostDBonus(actorData);
    data.modifDgts.waterD.max = data.modifDgts.waterD.base + this._waterDBonus(actorData);
    data.modifDgts.airD.max = data.modifDgts.airD.base + this._airDBonus(actorData);
    data.modifDgts.poisonD.max = data.modifDgts.poisonD.base + this._poisonDBonus(actorData);
    data.modifDgts.earthD.max = data.modifDgts.earthD.base + this._earthDBonus(actorData);
    data.modifDgts.natureD.max = data.modifDgts.natureD.base + this._natureDBonus(actorData);
    data.modifDgts.windD.max = data.modifDgts.windD.base + this._windDBonus(actorData);
    data.modifDgts.electricityD.max = data.modifDgts.electricityD.base + this._electricityDBonus(actorData);
    data.modifDgts.darkD.max = data.modifDgts.darkD.base + this._darkDBonus(actorData);
    data.modifDgts.lightD.max = data.modifDgts.lightD.base + this._lightDBonus(actorData);

    data.effect_cat.burn.max = data.effect_cat.burn.base + this._burnBonus(actorData);
    data.effect_cat.wet.max = data.effect_cat.wet.base + this._wetBonus(actorData);
    data.effect_cat.freeze.max = data.effect_cat.freeze.base + this._freezeBonus(actorData);
    data.effect_cat.paralysis.max = data.effect_cat.paralysis.base + this._paralysisBonus(actorData);
    data.effect_cat.slow.max = data.effect_cat.slow.base + this._slowBonus(actorData);
    data.effect_cat.flash.max = data.effect_cat.flash.base + this._flashBonus(actorData);
    data.effect_cat.dark.max = data.effect_cat.dark.base + this._darkBonus(actorData);
    data.effect_cat.poison.max = data.effect_cat.poison.base + this._poisonBonus(actorData);





    //Form Shift Calcs
    if (this._wereWolfForm(actorData) === true) {
      data.resistance.silverR = data.resistance.silverR - 5;
      data.resistance.diseaseR = data.resistance.diseaseR + 200;
      data.hp.max = data.hp.max + 5;
      data.stamina.max = data.stamina.max + 1;
      data.speed.base = data.speed.base + 9;
      data.speed.value = this._speedCalc(actorData);
      data.speed.swimSpeed = (data.speed.value/2).toFixed(0);
      data.resistance.natToughness = 5;
      data.wound_threshold.value = data.wound_threshold.value + 5;
      data.action_points.max = data.action_points.max - 1;
    } else if (this._wereBatForm(actorData) === true) {
        data.resistance.silverR = data.resistance.silverR - 5;
        data.resistance.diseaseR = data.resistance.diseaseR + 200;
        data.hp.max = data.hp.max + 5;
        data.stamina.max = data.stamina.max + 1;
        data.speed.value = (this._speedCalc(actorData)/2).toFixed(0);
        data.speed.flySpeed = data.speed.base + 9;
        data.speed.swimSpeed = (data.speed.value/2).toFixed(0);
        data.resistance.natToughness = 5;
        data.wound_threshold.value = data.wound_threshold.value + 3;
        data.action_points.max = data.action_points.max - 1;
    } else if (this._wereBoarForm(actorData) === true) {
        data.resistance.silverR = data.resistance.silverR - 5;
        data.resistance.diseaseR = data.resistance.diseaseR + 200;
        data.hp.max = data.hp.max + 5;
        data.speed.base = data.speed.base + 9;
        data.speed.value = this._speedCalc(actorData);
        data.speed.swimSpeed = (data.speed.value/2).toFixed(0);
        data.resistance.natToughness = 7;
        data.wound_threshold.value = data.wound_threshold.value + 5;
        data.action_points.max = data.action_points.max - 1;
    } else if (this._wereBearForm(actorData) === true) {
        data.resistance.silverR = data.resistance.silverR - 5;
        data.resistance.diseaseR = data.resistance.diseaseR + 200;
        data.hp.max = data.hp.max + 10;
        data.stamina.max = data.stamina.max + 1;
        data.speed.base = data.speed.base + 5;
        data.speed.value = this._speedCalc(actorData);
        data.speed.swimSpeed = (data.speed.value/2).toFixed(0);
        data.resistance.natToughness = 5;
        data.wound_threshold.value = data.wound_threshold.value + 5;
        data.action_points.max = data.action_points.max - 1;
    } else if (this._wereCrocodileForm(actorData) === true) {
        data.resistance.silverR = data.resistance.silverR - 5;
        data.resistance.diseaseR = data.resistance.diseaseR + 200;
        data.hp.max = data.hp.max + 5;
        data.stamina.max = data.stamina.max + 1;
        data.speed.value = (this._addHalfSpeed(actorData)).toFixed(0);
        data.speed.swimSpeed = parseFloat(this._speedCalc(actorData)) + 9;
        data.resistance.natToughness = 5;
        data.wound_threshold.value = data.wound_threshold.value + 5;
        data.action_points.max = data.action_points.max - 1;

    } else if (this._wereVultureForm(actorData) === true) {
        data.resistance.silverR = data.resistance.silverR - 5;
        data.resistance.diseaseR = data.resistance.diseaseR + 200;
        data.hp.max = data.hp.max + 5;
        data.stamina.max = data.stamina.max + 1;
        data.speed.value = (this._speedCalc(actorData)/2).toFixed(0);
        data.speed.flySpeed = data.speed.base + 9;
        data.speed.swimSpeed = (data.speed.value/2).toFixed(0);
        data.resistance.natToughness = 5;
        data.wound_threshold.value = data.wound_threshold.value + 3;
        data.action_points.max = data.action_points.max - 1;
    }else if (this._vampireLordForm(actorData) === true) {
        data.resistance.fireR = data.resistance.fireR - 1;
        data.resistance.sunlightR = data.resistance.sunlightR - 1;
        data.speed.flySpeed = 5;
        data.hp.max = data.hp.max + 5;
        data.magicka.max = data.magicka.max + 25;
        data.resistance.natToughness = 3;
    }

    //Speed Recalculation
    data.speed.value = this._addHalfSpeed(actorData);

    //ENC Burden Calculations
    if (game.settings.get('talthea-system', 'npcENCPenalty')) {
      if (data.carry_rating.current > data.carry_rating.max * 3) {
        data.carry_rating.label = 'Crushing'
        data.carry_rating.penalty = -40
        data.speed.value = 0;
        data.stamina.max = data.stamina.max - 5;
      } else if (data.carry_rating.current > data.carry_rating.max * 2) {
        data.carry_rating.label = 'Severe'
        data.carry_rating.penalty = -20
        data.speed.value = Math.floor(data.speed.base / 2);
        data.stamina.max = data.stamina.max - 3;
      } else if (data.carry_rating.current > data.carry_rating.max) {
        data.carry_rating.label = 'Moderate'
        data.carry_rating.penalty = -10
        data.speed.value = data.speed.value - 1;
        data.stamina.max = data.stamina.max - 1;
      } else if (data.carry_rating.current <= data.carry_rating.max) {
        data.carry_rating.label = "Minimal"
        data.carry_rating.penalty = 0
      }
    }

    //Armor Weight Class Calculations
    if (data.armor_class == "super_heavy") {
      data.speed.value = data.speed.value - 3;
      data.speed.swimSpeed = data.speed.swimSpeed - 3;
    } else if (data.armor_class == "heavy") {
      data.speed.value = data.speed.value - 2;
      data.speed.swimSpeed = data.speed.swimSpeed - 2;
    } else if (data.armor_class == "medium") {
      data.speed.value = data.speed.value - 1;
      data.speed.swimSpeed = data.speed.swimSpeed - 1;
    } else {
      data.speed.value = data.speed.value;
      data.speed.swimSpeed = data.speed.swimSpeed;
    }


    // Set Skill professions to regular professions (This is a fucking mess, but it's the way it's done for now...)
    for (let prof in data.professions) {
      if (prof === 'profession1'||prof === 'profession2'||prof === 'profession3'||prof === 'commerce') {
        data.professions[prof] === 0 ? data.professions[prof] = data.skills[prof].tn : data.professions[prof] = 0
      }
    }


    // Wound Penalties
    if (data.wounded === true) {
      let woundPen = 0
      let woundIni = -2;
      this._painIntolerant(actorData) ? woundPen = -30 : woundPen = -20

      if (this._halfWoundPenalty(actorData) === true) {
        for (var skill in data.professionsWound) {
          data.professionsWound[skill] = data.professions[skill] + (woundPen / 2);
        }

        data.woundPenalty = woundPen / 2
        data.initiative.value = data.initiative.base + (woundIni / 2);

      } 

      else if (this._halfWoundPenalty(actorData) === false) {
        for (var skill in data.professionsWound) {
          data.professionsWound[skill] = data.professions[skill] + woundPen;
        }

        data.initiative.value = data.initiative.base + woundIni;
        data.woundPenalty = woundPen;

        }
      } 
      
      else if (data.wounded === false) {
          for (var skill in data.professionsWound) {
           data.professionsWound[skill] = data.professions[skill];
        }
      }


    // Calculate Item Profession Modifiers
    this._calculateItemSkillModifiers(actorData)

  }

  async _calculateItemSkillModifiers(actorData) {
    let modItems = actorData.items.filter(i => 
      i.system.hasOwnProperty('skillArray')
      && i.system.skillArray.length > 0
      && i.system.equipped
    )

    for (let item of modItems) {
      for (let entry of item.system.skillArray) {
        let moddedSkill = actorData.system.professions[entry.name]
        actorData.system.professions[entry.name] = Number(moddedSkill) + Number(entry.value)
        actorData.system.professionsWound[entry.name] = Number(moddedSkill) + Number(entry.value)
      }
    }
  }
  // FORCE
  _forBonusCalc(actorData) {
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("characteristicBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.characteristicBonus.forChaBonus;
    }
    return totalBonus
  }
  _paradeBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.paradeBonus;
    }
    return totalBonus
  }
  _lancerBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.lancerBonus;
    }
    return totalBonus
  }
  _mains_nuesBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.mains_nuesBonus;
    }
    return totalBonus
  }
  _armes_moyennes_lourdesBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.armes_moyennes_lourdesBonus;
    }
    return totalBonus
  }
  _enduranceBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.enduranceBonus;
    }
    return totalBonus
  }
  _intimidationBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.intimidationBonus;
    }
    return totalBonus
  }

  //ENDURANCE
  _endBonusCalc(actorData) {
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("characteristicBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.characteristicBonus.endChaBonus;
    }
    return totalBonus
  }
  //AGILITE
  _agiBonusCalc(actorData) {
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("characteristicBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.characteristicBonus.agiChaBonus;
    }
    return totalBonus
  }
  _esquiveBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.esquiveBonus;
    }
    return totalBonus
  }
  _discretionBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.discretionBonus;
    }
    return totalBonus
  }
  _crochetageBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.crochetageBonus;
    }
    return totalBonus
  }
  _initiativeBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.initiativeBonus;
    }
    return totalBonus
  }
  _acrobatieBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.acrobatieBonus;
    }
    return totalBonus
  }
  _armes_courtes_distancesBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.armes_courtes_distancesBonus;
    }
    return totalBonus
  }

  //INTELLIGENCE
  _intBonusCalc(actorData) {
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("characteristicBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.characteristicBonus.intChaBonus;
    }
    return totalBonus
  }
  _histoireBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.histoireBonus;
    }
    return totalBonus
  }
  _languesBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.languesBonus;
    }
    return totalBonus
  }
  _alchimieBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.alchimieBonus;
    }
    return totalBonus
  }
  _medecineBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.medecineBonus;
    }
    return totalBonus
  }
  _arcanesBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.arcanesBonus;
    }
    return totalBonus
  }
  _natureBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.natureBonus;
    }
    return totalBonus
  }

  //WILLPOWER
  _wpBonusCalc(actorData) {
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("characteristicBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.characteristicBonus.wpChaBonus;
    }
    return totalBonus
  }

  //PERCEPTION
  _prcBonusCalc(actorData) {
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("characteristicBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.characteristicBonus.prcChaBonus;
    }
    return totalBonus
  }
  _reflexeBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.reflexeBonus;
    }
    return totalBonus
  }
  _ecouteBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.ecouteBonus;
    }
    return totalBonus
  }
  _detectionMBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.detectionMBonus;
    }
    return totalBonus
  }
  _reperageBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.reperageBonus;
    }
    return totalBonus
  }
  _discernementBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.discernementBonus;
    }
    return totalBonus
  }
  _pistageBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.pistageBonus;
    }
    return totalBonus
  }

  //
  _prsBonusCalc(actorData) {
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("characteristicBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.characteristicBonus.prsChaBonus;
    }
    return totalBonus
  }
  //LUCK
  _lckBonusCalc(actorData) {
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("characteristicBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.characteristicBonus.lckChaBonus;
    }
    return totalBonus
  }

  //SAGESSE
  _sagBonusCalc(actorData) {
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("characteristicBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.characteristicBonus.sagChaBonus;
    }
    return totalBonus
  }
  _concentrationBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.concentrationBonus;
    }
    return totalBonus
  }
  _intuitionBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.intuitionBonus;
    }
    return totalBonus
  }
  _survieBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.survieBonus;
    }
    return totalBonus
  }
  _premiers_soinsBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.premiers_soinsBonus;
    }
    return totalBonus
  }
  _intuitionMBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.intuitionMBonus;
    }
    return totalBonus
  }
  _artisanatsBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.artisanatsBonus;
    }
    return totalBonus
  }

  //ELOQUENCE
  _eloBonusCalc(actorData) {
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("characteristicBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.characteristicBonus.eloChaBonus;
    }
    return totalBonus
  }
  _charmeBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.charmeBonus;
    }
    return totalBonus
  }
  _spectacleBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.spectacleBonus;
    }
    return totalBonus
  }
  _bluffBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.bluffBonus;
    }
    return totalBonus
  }
  _mensongeBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.mensongeBonus;
    }
    return totalBonus
  }
  _negociationBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.negociationBonus;
    }
    return totalBonus
  }
  _trocBonusCalc(actorData){
    let forBonusItems = actorData.items.filter(item => item.system.hasOwnProperty("competenceBonus"));
    let totalBonus = 0;
    for (let item of forBonusItems) {
      totalBonus = totalBonus + item.system.competenceBonus.trocBonus;
    }
    return totalBonus
  }

  _calculateENC(actorData) {
    let weighted = actorData.items.filter(item => item.system.hasOwnProperty("enc"));
    let totalWeight = 0.0;
    for (let item of weighted) {
      totalWeight = totalWeight + (item.system.enc * item.system.quantity);
    }
    return totalWeight
  }

  _armorWeight(actorData) {
    let worn = actorData.items.filter(item => item.system.equipped == true);
    let armorENC = 0.0;
    for (let item of worn) {
      armorENC = armorENC + ((item.system.enc) * item.system.quantity);
    } 
    return armorENC
  }

  _excludeENC(actorData) {
    let excluded = actorData.items.filter(item => item.system.excludeENC == true);
    let totalWeight = 0.0;
    for (let item of excluded) {
      totalWeight = totalWeight + (item.system.enc * item.system.quantity);
    }
    return totalWeight
  }

  _hpBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("hpBonus"));
    let bonus = 0;
    for (let item of attribute) {
      bonus = bonus + item.system.hpBonus;
    }
    return bonus
  }

  _mpBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("mpBonus"));
    let bonus = 0;
    for (let item of attribute) {
      bonus = bonus + item.system.mpBonus;
    }
    return bonus
  }

  _spBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("spBonus"));
    let bonus = 0;
    for (let item of attribute) {
      bonus = bonus + item.system.spBonus;
    }
    return bonus
  }

  _lpBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("lpBonus"));
    let bonus = 0;
    for (let item of attribute) {
      bonus = bonus + item.system.lpBonus;
    }
    return bonus
  }

  _wtBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("wtBonus"));
    let bonus = 0;
    for (let item of attribute) {
      bonus = bonus + item.system.wtBonus;
    }
    return bonus
  }

  _speedBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("speedBonus"));
    let bonus = 0;
    for (let item of attribute) {
      bonus = bonus + item.system.speedBonus;
    }
    return bonus
  }

  _iniBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("iniBonus"));
    let bonus = 0;
    for (let item of attribute) {
      bonus = bonus + item.system.iniBonus;
    }
    return bonus
  }

  _diseaseR(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("diseaseR"));
    let bonus = 0;
    for (let item of attribute) {
      bonus = bonus + item.system.diseaseR;
    }
    return bonus
  }

  _fireR(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("fireR"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.fireR;
      }
      return bonus
  }

  _frostR(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("frostR"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.frostR;
      }
      return bonus
  }

  _shockR(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("shockR"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.shockR;
      }
      return bonus
  }

  _poisonR(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("poisonR"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.poisonR;
      }
      return bonus
  }

  _magicR(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("magicR"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.magicR;
      }
      return bonus
  }

  _natToughnessR(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("natToughnessR"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.natToughnessR;
      }
      return bonus
  }

  _silverR(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("silverR"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.silverR;
      }
      return bonus
  }

  _sunlightR(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("sunlightR"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.sunlightR;
      }
      return bonus
  }

    //Resistances
  _fireRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("fireRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.fireRBonus;
      }
      return bonus
  }

  _frostRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("frostRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.frostRBonus;
      }
      return bonus
  }

  _poisonRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("poisonRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.poisonRBonus;
      }
      return bonus
  }

  _magicRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("magicRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.magicRBonus;
      }
      return bonus
  }

  _physicRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("physicRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.physicRBonus;
      }
      return bonus
  }

  _waterRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("waterRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.waterRBonus;
      }
      return bonus
  }

  _airRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("airRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.airRBonus;
      }
      return bonus
  }

  _earthRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("earthRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.earthRBonus;
      }
      return bonus
  }

  _natureRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("natureRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.natureRBonus;
      }
      return bonus
  }

  _windRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("windRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.windRBonus;
      }
      return bonus
  }

  _electricityRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("electricityRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.electricityRBonus;
      }
      return bonus
  }

  _darkRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("darkRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.darkRBonus;
      }
      return bonus
  }

  _lightRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("lightRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.lightRBonus;
      }
      return bonus
  }

  _burnBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("burnBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.burnBonus;
      }
      return bonus
  }

  _wetBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("wetBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.wetBonus;
      }
      return bonus
  }

  _freezeBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("freezeBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.freezeBonus;
      }
      return bonus
  }

  _paralysisBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("paralysisBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.paralysisBonus;
      }
      return bonus
  }

  _slowBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("slowBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.slowBonus;
      }
      return bonus
  }

  _flashBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("flashBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.flashBonus;
      }
      return bonus
  }

  _darkBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("darkBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.darkBonus;
      }
      return bonus
  }

  _poisonBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("poisonBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.poisonBonus;
      }
      return bonus
  }

  _shockRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("shockRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.shockRBonus;
      }
      return bonus
  }

  //Modif dgts
  _fireDBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("fireDBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.fireDBonus;
      }
      return bonus
  }

  _frostDBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("frostDBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.frostDBonus;
      }
      return bonus
  }

  _poisonDBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("poisonDBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.poisonDBonus;
      }
      return bonus
  }

  _magicDBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("magicDBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.magicDBonus;
      }
      return bonus
  }

  _physicDBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("physicDBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.physicDBonus;
      }
      return bonus
  }

  _waterDBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("waterDBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.waterDBonus;
      }
      return bonus
  }

  _airDBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("airDBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.airDBonus;
      }
      return bonus
  }

  _earthDBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("earthDBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.earthDBonus;
      }
      return bonus
  }

  _natureDBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("natureDBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.natureDBonus;
      }
      return bonus
  }

  _windDBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("windDBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.windDBonus;
      }
      return bonus
  }

  _electricityDBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("electricityDBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.electricityDBonus;
      }
      return bonus
  }

  _darkDBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("darkDBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.darkDBonus;
      }
      return bonus
  }

  _lightDBonus(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("lightDBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.system.lightDBonus;
      }
      return bonus
  }

  _swimCalc(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("swimBonus"));
    let bonus = 0;
    for (let item of attribute) {
      bonus = bonus + item.system.swimBonus;
    }
    return bonus
  }

  _flyCalc(actorData) {
    let attribute = actorData.items.filter(item => item.system.hasOwnProperty("flyBonus"));
    let bonus = 0;
    for (let item of attribute) {
      bonus = bonus + item.system.flyBonus;
    }
    return bonus
  }

  _speedCalc(actorData) {
    let attribute = actorData.items.filter(item => item.system.halfSpeed === true);
    let speed = actorData.system.speed.base;
    if (attribute.length === 0) {
      speed = speed;
    } else if (attribute.length >= 1) {
      speed = Math.ceil(speed/2);
    }
    return speed;
  }

  _iniCalc(actorData) {
    let attribute = actorData.items.filter(item => item.type == "trait"|| item.type == "talent");
    let init = actorData.system.initiative.base;
      for (let item of attribute) {
        if (item.system.replace.ini.characteristic != "none") {
          if (item.system.replace.ini.characteristic == "for") {
            init = Math.floor(actorData.system.characteristics.for.total / 10) * 3;
          } else if (item.system.replace.ini.characteristic == "end") {
            init = Math.floor(actorData.system.characteristics.end.total / 10) * 3;
          } else if (item.system.replace.ini.characteristic == "agi") {
            init = Math.floor(actorData.system.characteristics.agi.total / 10) * 3;
          } else if (item.system.replace.ini.characteristic == "int") {
            init = Math.floor(actorData.system.characteristics.int.total / 10) * 3;
          } else if (item.system.replace.ini.characteristic == "wp") {
            init = Math.floor(actorData.system.characteristics.wp.total / 10) * 3;
          } else if (item.system.replace.ini.characteristic == "prc") {
            init = Math.floor(actorData.system.characteristics.prc.total / 10) * 3;
          } else if (item.system.replace.ini.characteristic == "prs") {
            init = Math.floor(actorData.system.characteristics.prs.total / 10) * 3;
          } else if (item.system.replace.ini.characteristic == "lck") {
            init = Math.floor(actorData.system.characteristics.lck.total / 10) * 3;
          } else if (item.system.replace.ini.characteristic == "sag") {
            init = Math.floor(actorData.system.characteristics.sag.total / 10) * 3;
          } else if (item.system.replace.ini.characteristic == "elo") {
            init = Math.floor(actorData.system.characteristics.elo.total / 10) * 3;
          }
        }
      }
    return init;
  }

  _woundThresholdCalc(actorData) {
    let attribute = actorData.items.filter(item => item.type === "trait"|| item.type === "talent");
    let wound = actorData.system.wound_threshold.base;
      for (let item of attribute) {
        if (item.system.replace.wt.characteristic != "none") {
          if (item.system.replace.wt.characteristic === "for") {
            wound = Math.floor(actorData.system.characteristics.for.total / 10) * 3;
          } else if (item.system.replace.wt.characteristic === "end") {
            wound = Math.floor(actorData.system.characteristics.end.total / 10) * 3;
          } else if (item.system.replace.wt.characteristic === "agi") {
            wound = Math.floor(actorData.system.characteristics.agi.total / 10) * 3;
          } else if (item.system.replace.wt.characteristic === "int") {
            wound = Math.floor(actorData.system.characteristics.int.total / 10) * 3;
          } else if (item.system.replace.wt.characteristic === "wp") {
            wound = Math.floor(actorData.system.characteristics.wp.total / 10) * 3;
          } else if (item.system.replace.wt.characteristic === "prc") {
            wound = Math.floor(actorData.system.characteristics.prc.total / 10) * 3;
          } else if (item.system.replace.wt.characteristic === "prs") {
            wound = Math.floor(actorData.system.characteristics.prs.total / 10) * 3;
          } else if (item.system.replace.wt.characteristic === "lck") {
            wound = Math.floor(actorData.system.characteristics.lck.total / 10) * 3;
          } else if (item.system.replace.wt.characteristic === "sag") {
            wound = Math.floor(actorData.system.characteristics.sag.total / 10) * 3;
          } else if (item.system.replace.wt.characteristic === "elo") {
            wound = Math.floor(actorData.system.characteristics.elo.total / 10) * 3;
          }
        }
      }
    return wound;
  }


  _halfWoundPenalty(actorData) {
    let attribute = actorData.items.filter(item => item.system.halfWoundPenalty == true);
    let woundReduction = false;
    if (attribute.length >= 1) {
      woundReduction = true;
    } else {
      woundReduction = false;
    }
    return woundReduction
  }

  _addIntToMP(actorData) {
    let attribute = actorData.items.filter(item => item.system.addIntToMP == true);
    let mp = 0;
    if (attribute.length >= 1) {
      mp = actorData.system.characteristics.int.total;
    } else {
      mp = 0;
    }
    return mp
  }

  _untrainedException(actorData) {
    let attribute = actorData.items.filter(item => item.system.untrainedException == true);
    const legacyUntrained = game.settings.get("talthea-system", "legacyUntrainedPenalty");
    let x = 0;
    if (legacyUntrained) {
      if (attribute.length >= 1) {
        x = 20;
      }
    } else if (attribute.length >= 1) {
      x = 10;
    }
    return x
  }

  _isMechanical(actorData) {
    let attribute = actorData.items.filter(item => item.system.mechanical == true);
    let isMechanical = false;
    if (attribute.length >= 1) {
      isMechanical = true;
    } else {
      isMechanical = false;
    }
    return isMechanical
  }

  _dwemerSphere(actorData) {
    let attribute = actorData.items.filter(item => item.system.shiftForm == true);
    let shift = false;
    if (attribute.length >= 1) {
      for (let item of attribute) {
        if (item.system.dailyUse == true) {
          shift = true;
        }
      }
    } else {
      shift = false;
    }
    return shift
  }

  _vampireLordForm(actorData) {
    let form = actorData.items.filter(item => item.system.shiftFormStyle === "shiftFormVampireLord");
    let shift = false;
    if(form.length > 0) {
      shift = true;
    }
    return shift
  }

  _wereWolfForm(actorData) {
    let form = actorData.items.filter(item => item.system.shiftFormStyle === "shiftFormWereWolf"||item.system.shiftFormStyle === "shiftFormWereLion");
    let shift = false;
    if(form.length > 0) {
      shift = true;
    }
    return shift
  }

  _wereBatForm(actorData) {
    let form = actorData.items.filter(item => item.system.shiftFormStyle === "shiftFormWereBat");
    let shift = false;
    if(form.length > 0) {
      shift = true;
    }
    return shift
  }

  _wereBoarForm(actorData) {
    let form = actorData.items.filter(item => item.system.shiftFormStyle === "shiftFormWereBoar");
    let shift = false;
    if(form.length > 0) {
      shift = true;
    }
    return shift
  }

  _wereBearForm(actorData) {
    let form = actorData.items.filter(item => item.system.shiftFormStyle === "shiftFormWereBear");
    let shift = false;
    if(form.length > 0) {
      shift = true;
    }
    return shift
  }

  _wereCrocodileForm(actorData) {
    let form = actorData.items.filter(item => item.system.shiftFormStyle === "shiftFormWereCrocodile");
    let shift = false;
    if(form.length > 0) {
      shift = true;
    }
    return shift
  }

  _wereVultureForm(actorData) {
    let form = actorData.items.filter(item => item.system.shiftFormStyle === "shiftFormWereVulture");
    let shift = false;
    if(form.length > 0) {
      shift = true;
    }
    return shift
  }

  _painIntolerant(actorData) {
    let attribute = actorData.items.filter(item => item.system.painIntolerant == true);
    let pain = false;
    if (attribute.length >= 1) {
      pain = true;
    } 
    return pain
  }

  _addHalfSpeed(actorData) {
    let halfSpeedItems = actorData.items.filter(item => item.system.addHalfSpeed === true);
    let isWereCroc = actorData.items.filter(item => item.system.shiftFormStyle === "shiftFormWereCrocodile");
    let speed = actorData.system.speed.value;
    if (isWereCroc.length > 0 && halfSpeedItems.length > 0) {
      speed = actorData.system.speed.base;
    } else if (isWereCroc.length == 0 && halfSpeedItems.length > 0) {
      speed = Math.ceil(actorData.system.speed.value/2) + actorData.system.speed.base;
    } else if (isWereCroc.length > 0 && halfSpeedItems.length == 0) {
      speed = Math.ceil(actorData.system.speed.base/2);
    } else {
      speed = actorData.system.speed.value;
    }
    return speed
  }

}
